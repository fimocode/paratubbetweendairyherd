//
//  main.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 13/12/2013.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include <iostream>

#include "Simulation.h"
#include "Parameters.h"

int main(int argc, const char * argv[])
{
    //    std::cout << std::endl << "\033[1;31mParatubBetweenDairyHerd - MultiThreaded (v.d28m04a2015)\033[1;m" << std::endl;
    std::cout << std::endl << "ParatubBetweenDairyHerd - MultiThreaded (v.d09m06a2016)" << std::endl << std::endl;

    Parameters& ParamMain = Parameters::get_instance(argc, argv);
    
    Simulation iSimulation;
    iSimulation.Simulate();

    gsl_rng_free (ParamMain.randomGenerator);
    
    return 0;
}

