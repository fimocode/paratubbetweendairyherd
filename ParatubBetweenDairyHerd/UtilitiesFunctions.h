//
//  UtilitiesFunctions.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 26/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_UtilitiesFunctions_h
#define ParatubBetweenDairyHerd_UtilitiesFunctions_h

#include <iostream>
#include <vector>
#include <random>
#include <numeric>
#include <assert.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

namespace U_functions {
    // Matrix 2D sum function
    int SumOfAnimalsByAge(const std::vector<int>& vect, int age1, int age2);
    
    int SumOfAnimalsByAge(const std::vector<unsigned short int>& vect, int age1, int age2);
    
    // Vector division, member by member
    std::vector<double> VectorDivision(const std::vector<double>& vector1, const std::vector<double>& vector2);

    std::vector<double> VectorDivision(const std::vector<double>& vector1, const double& denum);
    std::vector<double> VectorDivision(const std::vector<double>& vector1, const int& denum);
    
    std::vector<double> VectorDivision(const std::vector<int>& vector1, const std::vector<int>& vector2);
    
    // Vector sum
    double VectorSum(const std::vector<double>& v, int index1, int index2);
    
    void multinomialDistribution(gsl_rng * randomGenerator, const unsigned int N, const std::vector<double>& vProbs, std::vector<int>& vResults);
    
    void multinomialDistribution(gsl_rng * randomGenerator, const unsigned int N, const std::vector<double>& vProbs, std::vector<short int>& vResults);
    
    std::vector<int> multinomialDistribution(gsl_rng * randomGenerator, const unsigned int N, const std::vector<double>& vProbs);
    
    std::vector<unsigned short int> multinomialDistribution_shortInt(gsl_rng * randomGenerator, const unsigned int N, const std::vector<double>& vProbs);
    
//    std::vector<int> RandomSamplingWithoutReplacement(std::vector<int>& vData, int N);
    
    std::vector<int> RandomSamplingWithoutReplacement(gsl_rng * randomGenerator, std::vector<int>& vData, int N);

    std::vector<int> RandomSamplingHealthStatesWithoutReplacement(gsl_rng * randomGenerator, const std::vector<int>& vData, int N);
    
    std::vector<int> ListAgeOfAnimals(const std::vector<int>& headcountByAge);
    
    std::vector<int> ListHealthStatesOfAnimals(const std::vector<int>& headcountByHealthStates);
    
    std::vector<double> getProportionsFromHeadcount(const std::vector<int>& headcount);
    
    std::vector<double> getProportionsFromHeadcount(const std::vector<unsigned short int>& headcount);
    
    std::vector<double> getZeroAndOneFromVector(const std::vector<unsigned short int>& vVector);
    
    std::vector<double> VectorMultiplication(const std::vector<double>& vector1, const std::vector<double>& vector2);
    
    double gaussianPdf(const double& x, const double& mu, const double& sigma);
    
}

#endif

