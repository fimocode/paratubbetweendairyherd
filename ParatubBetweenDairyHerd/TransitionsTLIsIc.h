//
//  TransitionsTLIsIc.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 05/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_TransitionsTLIsIc_h
#define ParatubBetweenDairyHerd_TransitionsTLIsIc_h

#include <gsl/gsl_randist.h>
#include <vector>

#include "Parameters.h"
#include "HealthStateCompartmentsContainer.hpp"
#include "SummaryInfection.h"

class TransitionsTLIsIc {
    // Variables ===========================================================
    int newIc;
    int newIs;
    int newL;
    
    int newIc_positiveTested;
    int newIs_positiveTested;
    int newL_positiveTested;
    
public:
    // Constructor ==========================================================
    TransitionsTLIsIc();
    
    // Member functions ====================================================
    void update(gsl_rng * randomGenerator, const int& t, const int& age, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
    
    void transfer(const int& t, const int& age, HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer, SummaryInfection& DynInf);
    
};

#endif
