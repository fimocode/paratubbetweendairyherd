//
//  BufferGlobalResults.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 08/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_BufferGlobalResults_h
#define ParatubBetweenDairyHerd_BufferGlobalResults_h

#include <vector>
#include <boost/accumulators/numeric/functional/vector.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/sum.hpp>
#include <boost/accumulators/statistics/count.hpp>

#include "Parameters.h"
#include "DairyHerd.h"
#include "MetapopSummary.h"

class BufferGlobalResults {
public:
    // Constructor =========================================================
    BufferGlobalResults();
    
    void clear();
    
    // Variables ===========================================================
    std::vector<int> vMetapopPersistence; //
    
    // Member functions ====================================================
//    void updateBuffer();
    
};

#endif
