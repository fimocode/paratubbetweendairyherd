//
//  MapInEnvironments.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 05/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "MapInEnvironments.h"

// Constructor =========================================================
MapInEnvironments::MapInEnvironments() {
    int_1.resize(Parameters::simutime,0);
    int_2.resize(Parameters::simutime,0);
    int_3.resize(Parameters::simutime,0);
    int_4.resize(Parameters::simutime,0);
    int_5.resize(Parameters::simutime,0);
    global.resize(Parameters::simutime,0);
    
    ext_1.resize(Parameters::simutime,0);
    ext_2.resize(Parameters::simutime,0);
    ext_3.resize(Parameters::simutime,0);
    ext.resize(Parameters::simutime,0);
    
    milk.resize(Parameters::simutime,0);
    colostrum.resize(Parameters::simutime,0);
}


// Member functions ====================================================
void MapInEnvironments::update(gsl_rng * randomGenerator, const int& t, const int& weeknumber, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const MapShedding& QtyBacteria, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer, BirthsContainer& iBirthsContainer){
    // Env. interior and exterior --------------------------------------
    // non weaned calf
    int_1[t] = int_1[t-1] * (1-iParametersSpecificToEachHerd.deathRateBactInsideInThisFarm) + QtyBacteria.QtyTns;
    if ( (iHealthStateCompartmentsContainer.iCompartmentSR.calvesUnweaned[t-1] + iHealthStateCompartmentsContainer.iCompartmentT.calvesUnweaned[t-1] + iHealthStateCompartmentsContainer.iCompartmentL.calvesUnweaned[t-1] + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.calvesUnweaned[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.calvesUnweaned[t-1] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.calvesUnweaned[t-1]) > 0 ) {
        cleaningindicator1 = 1;
    } else if ( cleaningindicator1 == 1 ) { // caution, cleaning is done only one time after pen were empty
        int_1[t] *= (1-Parameters::cleaningCP);
        cleaningindicator1 = 0;
    }
    
    
    if ((weeknumber >= Parameters::startGrazing) && (weeknumber <= Parameters::endGrazing)) {
        // - in the grazing period
        // weaned calf
        int_2[t] = int_2[t-1] * (1-iParametersSpecificToEachHerd.deathRateBactInsideInThisFarm) + QtyBacteria.QtyTs1;
        if ( (iHealthStateCompartmentsContainer.iCompartmentSR.calvesWeanedIn[t-1] + iHealthStateCompartmentsContainer.iCompartmentT.calvesWeanedIn[t-1] + iHealthStateCompartmentsContainer.iCompartmentL.calvesWeanedIn[t-1] + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.calvesWeanedIn[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.calvesWeanedIn[t-1] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.calvesWeanedIn[t-1]) > 0 ) {
            cleaningindicator2 = 1;
        } else if ( cleaningindicator2 == 1 ) { // caution, cleaning is done only one time after pen were empty
            int_2[t] *= (1-Parameters::cleaningCP);
            cleaningindicator2 = 0;
        }
        // young heifer
        int_3[t] = int_3[t-1] * (1-iParametersSpecificToEachHerd.deathRateBactInsideInThisFarm);
        // heifer
        int_4[t] = int_4[t-1] * (1-iParametersSpecificToEachHerd.deathRateBactInsideInThisFarm);
        // adult
        int_5[t] = int_5[t-1] * (1-iParametersSpecificToEachHerd.deathRateBactInsideInThisFarm);
        
        ext_1[t] = ext_1[t-1] * (1-Parameters::deathRateBactOutside) + QtyBacteria.QtyTs2;
        ext_2[t] = ext_2[t-1] * (1-Parameters::deathRateBactOutside) + QtyBacteria.QtyTy;
        ext_3[t] = ext_3[t-1] * (1-Parameters::deathRateBactOutside) + QtyBacteria.QtyTg + QtyBacteria.QtyIsg + QtyBacteria.QtyIcg;
        
    } else {
        // - out of the grazing period
        // weaned calf
        int_2[t] = int_2[t-1] * (1-iParametersSpecificToEachHerd.deathRateBactInsideInThisFarm) + QtyBacteria.QtyTs1 + QtyBacteria.QtyTs2;
        if ( (iHealthStateCompartmentsContainer.iCompartmentSR.calvesWeanedOut[t-1] + iHealthStateCompartmentsContainer.iCompartmentT.calvesWeanedOut[t-1] + iHealthStateCompartmentsContainer.iCompartmentL.calvesWeanedOut[t-1] + iHealthStateCompartmentsContainer.iCompartmentSR_positiveTested.calvesWeanedOut[t-1] + iHealthStateCompartmentsContainer.iCompartmentT_positiveTested.calvesWeanedOut[t-1] + iHealthStateCompartmentsContainer.iCompartmentL_positiveTested.calvesWeanedOut[t-1]) > 0 ) {
            cleaningindicator2 = 1;
        } else if ( cleaningindicator2 == 1 ) { // caution, cleaning is done only one time after pen were empty
            int_2[t] *= (1-Parameters::cleaningCP);
            cleaningindicator2 = 0;
        }
        // young heifer
        int_3[t] = int_3[t-1] * (1-iParametersSpecificToEachHerd.deathRateBactInsideInThisFarm) + QtyBacteria.QtyTy;
        // heifer
        int_4[t] = int_4[t-1] * (1-iParametersSpecificToEachHerd.deathRateBactInsideInThisFarm) + QtyBacteria.QtyTg + QtyBacteria.QtyIsg + QtyBacteria.QtyIcg;
        // adult
        int_5[t] = int_5[t-1] * (1-iParametersSpecificToEachHerd.deathRateBactInsideInThisFarm) + QtyBacteria.QtyIs + QtyBacteria.QtyIc;
        
        ext_1[t] = 0; ext_2[t] = 0; ext_3[t] = 0;
    }
    
    
    // Global environment ----------------------------------------------
    global[t] = int_1[t] + int_2[t] + int_3[t] + int_4[t] + int_5[t];
    
    
    // Env. ext. -------------------------------------------------------
    ext[t] = ext_1[t] + ext_2[t] + ext_3[t];
    
    
    // Milk ------------------------------------------------------------
    if (Parameters::milkTR == true) {
        if (QtyBacteria.milkTot > 0) {
            milk[t] = Parameters::qtyMilkDrunk * (QtyBacteria.QtyMilkIs + QtyBacteria.QtyMilkIc) / QtyBacteria.milkTot;
        } else {
            milk[t] = 0;
        }
    }
    
    
    // Colostrum -------------------------------------------------------
    if (Parameters::colostrumTR == true) {
        int calvesIsColoRisk = gsl_ran_binomial(randomGenerator, Parameters::propExcreColoIs, iBirthsContainer.iBirthIs.birthsS());
        int calvesIcColoRisk = gsl_ran_binomial(randomGenerator, Parameters::propExcreColoIc, iBirthsContainer.iBirthIc.birthsS());
        
        colostrum.clear();
        colostrum.resize(calvesIsColoRisk + calvesIcColoRisk, 0);
        
        // Is
        for (int i = 0; i<(calvesIsColoRisk); i++) {
            colostrum[i] = Parameters::qtyColoDrunk * ( (100 * pow(10, 3) * gsl_ran_beta(randomGenerator, Parameters::alphaColoDirIs, Parameters::betaColoDirIs)) + (1 + 1000 * gsl_ran_beta(randomGenerator, Parameters::alphaColoIndirIs, Parameters::betaColoIndirIs)) );
        }
        // Ic
        for (int i = (calvesIsColoRisk); i<(calvesIsColoRisk + calvesIcColoRisk); i++) {
            colostrum[i] = Parameters::qtyColoDrunk * ( (100 * pow(10, 3) * gsl_ran_beta(randomGenerator, Parameters::alphaColoDirIc, Parameters::betaColoDirIc)) + pow(10, (3+10*gsl_ran_beta(randomGenerator, Parameters::alphaColoIndirIc, Parameters::betaColoIndirIc))) );
        }
        
    }
    
}

