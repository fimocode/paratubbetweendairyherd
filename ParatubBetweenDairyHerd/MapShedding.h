//
//  MapShedding.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 04/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_MapShedding_h
#define ParatubBetweenDairyHerd_MapShedding_h

#include <iostream>
#include <gsl/gsl_randist.h>
#include <vector>
#include <algorithm>

#include "Parameters.h"
#include "HealthStateCompartmentsContainer.hpp"


class MapShedding {
    // Variables ===========================================================
    
public:
    // Variables ===========================================================
    double QtyTns;
    double QtyTs1;
    double QtyTs2;
    double QtyTy;
    double QtyTg;
    
    double QtyIsg;
    double QtyIs;
    double QtyIcg;
    double QtyIc;
        
    double milkTot;
    double QtyMilkIs;
    double QtyMilkIc;
        
    // Constructor =========================================================
    MapShedding();
    
    // Member functions ====================================================
    void update(gsl_rng * randomGenerator, const int& t, const HealthStateCompartmentsContainer& iHealthStateCompartmentsContainer);
};

#endif
