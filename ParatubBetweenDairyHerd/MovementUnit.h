//
//  MovementUnit.h
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunee on 26/04/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#ifndef __ParatubBetweenDairyHerd__MovementUnit__
#define __ParatubBetweenDairyHerd__MovementUnit__

#include <stdio.h>
#include <string>

class MovementUnit {
    
public:
    // Constructor =========================================================
    MovementUnit();
    MovementUnit(const std::string& theDestination, const int& theAge);

    // Variables ===========================================================
    std::string destination;
    int age;

    // Member functions ====================================================
    std::string getDestination();
    int getAge();
    
};

#endif /* defined(__ParatubBetweenDairyHerd__MovementUnit__) */
