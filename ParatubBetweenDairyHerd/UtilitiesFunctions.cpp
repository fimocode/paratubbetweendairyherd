//
//  UtilitiesFunctions.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 26/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "UtilitiesFunctions.h"

int U_functions::SumOfAnimalsByAge(const std::vector<int>& vect, int age1, int age2){
//     int sum = 0;
//     for (int age = age1-1; age < age2; age++) {
//         sum += vect[age];
//     }

//    int sum = 0;
//    std::for_each(vect.begin()+age1-1, vect.begin()+age2, [&] (int n) {
//        sum += n;
//    });
//    return sum;

    return std::accumulate(vect.begin()+age1-1, vect.begin()+age2, 0);
}


int U_functions::SumOfAnimalsByAge(const std::vector<unsigned short int>& vect, int age1, int age2){
//     int sum = 0;
//     for (int age = age1-1; age < age2; age++) {
//         sum += vect[age];
//     }

//    int sum = 0;
//    std::for_each(vect.begin()+age1-1, vect.begin()+age2, [&] (int n) {
//        sum += n;
//    });
//    return sum;

    return std::accumulate(vect.begin()+age1-1, vect.begin()+age2, 0);
}



std::vector<double> U_functions::VectorDivision(const std::vector<double>& vector1, const std::vector<double>& vector2){
    assert(vector1.size() == vector2.size());
    std::vector<double> vector3; vector3.resize(vector1.size(),0);
    for (int i = 0; i<vector1.size(); i++) {
        if (vector2[i]==0) {
            vector3[i]=0;
        } else {
            vector3[i] = static_cast<double>(vector1[i]) / static_cast<double>(vector2[i]);
        }
    }
    return vector3;
}


std::vector<double> U_functions::VectorDivision(const std::vector<double>& vector1, const double& denum){
    std::vector<double> vector3; vector3.resize(vector1.size(),0);
    for (int i = 0; i<vector1.size(); i++) {
        if (denum==0) {
            vector3[i]=0;
        } else {
            vector3[i] = static_cast<double>(vector1[i]) / static_cast<double>(denum);
        }
    }
    return vector3;
}


std::vector<double> U_functions::VectorDivision(const std::vector<double>& vector1, const int& denum){
    std::vector<double> vector3; vector3.resize(vector1.size(),0);
    for (int i = 0; i<vector1.size(); i++) {
        if (denum==0) {
            vector3[i]=0;
        } else {
            vector3[i] = static_cast<double>(vector1[i]) / static_cast<double>(denum);
        }
    }
    return vector3;
}


std::vector<double> U_functions::VectorDivision(const std::vector<int>& vector1, const std::vector<int>& vector2){
    assert(vector1.size() == vector2.size());
    std::vector<double> vector3; vector3.resize(vector1.size(),0);
    for (int i = 0; i<vector1.size(); i++) {
        if (vector2[i]==0) {
            vector3[i]=0;
        } else {
            vector3[i] = static_cast<double>(vector1[i]) / static_cast<double>(vector2[i]);
        }
    }
    return vector3;
}



double U_functions::VectorSum(const std::vector<double>& v, int index1, int index2){
    double sum = 0;
    for (int index=index1; index<=index2; index++) {
        sum += v[index];
    }
    return sum;
}



void U_functions::multinomialDistribution(gsl_rng * randomGenerator, const unsigned int N, const std::vector<double>& vProbs, std::vector<int>& vResults)
{
    int i=0;
    //    double norm = 0.0;
    double sum_proba = 0.0;
    unsigned int sum_results = 0;
    
    double norm = std::accumulate(vProbs.begin(), vProbs.end(), 0.0); //for (i = 0; i < vProbs.size(); i++) norm += vProbs[i];
    
    for (i = 0; i < vProbs.size(); i++){
        if(vProbs[i] > 0.0){
            vResults[i] = gsl_ran_binomial(randomGenerator, vProbs[i] / (norm - sum_proba), N - sum_results);
        }else{
            vResults[i] = 0;
        }
        
        sum_proba += vProbs[i];
        sum_results += vResults[i];
        
        if (N - sum_results == 0) {
            break;
        }
    }
}



void U_functions::multinomialDistribution(gsl_rng * randomGenerator, const unsigned int N, const std::vector<double>& vProbs, std::vector<short int>& vResults)
{
    int i=0;
    //    double norm = 0.0;
    double sum_proba = 0.0;
    unsigned int sum_results = 0;
    
    double norm = std::accumulate(vProbs.begin(), vProbs.end(), 0.0); //for (i = 0; i < vProbs.size(); i++) norm += vProbs[i];
    
    for (i = 0; i < vProbs.size(); i++){
        if(vProbs[i] > 0.0){
            vResults[i] = gsl_ran_binomial(randomGenerator, vProbs[i] / (norm - sum_proba), N - sum_results);
        }else{
            vResults[i] = 0;
        }
        
        sum_proba += vProbs[i];
        sum_results += vResults[i];
        
        if (N - sum_results == 0) {
            break;
        }
    }
}



std::vector<int> U_functions::multinomialDistribution(gsl_rng * randomGenerator, const unsigned int N, const std::vector<double>& vProbs)
{
    std::vector<int> vResults; vResults.resize(vProbs.size(),0);
    
    int i=0;
    //    double norm = 0.0;
    double sum_proba = 0.0;
    unsigned int sum_results = 0;
    
    double norm = std::accumulate(vProbs.begin(), vProbs.end(), 0.0); //for (i = 0; i < vProbs.size(); i++) norm += vProbs[i];
    
    for (i = 0; i < vProbs.size(); i++){
        if(vProbs[i] > 0.0){
            vResults[i] = gsl_ran_binomial(randomGenerator, vProbs[i] / (norm - sum_proba), N - sum_results);
        }else{
            vResults[i] = 0;
        }
        
        sum_proba += vProbs[i];
        sum_results += vResults[i];
        
        if (N - sum_results == 0) {
            break;
        }
    }
    return vResults;
}



std::vector<unsigned short int> U_functions::multinomialDistribution_shortInt(gsl_rng * randomGenerator, const unsigned int N, const std::vector<double>& vProbs)
{
    std::vector<unsigned short int> vResults; vResults.resize(vProbs.size(),0);
    
    int i=0;
    //    double norm = 0.0;
    double sum_proba = 0.0;
    unsigned int sum_results = 0;
    
    double norm = std::accumulate(vProbs.begin(), vProbs.end(), 0.0); //for (i = 0; i < vProbs.size(); i++) norm += vProbs[i];
    
    for (i = 0; i < vProbs.size(); i++){
        if(vProbs[i] > 0.0){
            vResults[i] = gsl_ran_binomial(randomGenerator, vProbs[i] / (norm - sum_proba), N - sum_results);
        }else{
            vResults[i] = 0;
        }
        
        sum_proba += vProbs[i];
        sum_results += vResults[i];
        
        if (N - sum_results == 0) {
            break;
        }
    }
    return vResults;
}


//std::vector<int> U_functions::RandomSamplingWithoutReplacement(std::vector<int>& vData, int N){
//    // Random generator
//    std::random_device rd;
//    std::mt19937 randomGenerator(rd());
//    
//    int max = static_cast<int>(vData.size()-1);
//    std::vector<int> vResult;
//    
//    for (int n=1; n<=N; n++) {
//        std::uniform_int_distribution<> uniformDistribution(0,max);
//        int index = uniformDistribution(randomGenerator);
//        std::swap(vData[index],vData[max]);
//        vResult.emplace_back(vData[max]);
//        max--;
//    }
//    
//    return vResult;
//}

std::vector<int> U_functions::RandomSamplingWithoutReplacement(gsl_rng * randomGenerator, std::vector<int>& vData, int N){
    int max = static_cast<int>(vData.size()-1);
    std::vector<int> vResult;
    
    for (int n=1; n<=N; n++) {
        int index = static_cast<int>(gsl_rng_uniform_int(randomGenerator, max+1));
        std::swap(vData[index],vData[max]);
        vResult.emplace_back(vData[max]);
        max--;
    }
    
    return vResult;
}

std::vector<int> U_functions::RandomSamplingHealthStatesWithoutReplacement(gsl_rng * randomGenerator, const std::vector<int>& vData, int N){

    std::vector<int> vHealthStatesList = U_functions::ListHealthStatesOfAnimals(vData);
    std::vector<int> vHealthStatesBuffer = U_functions::RandomSamplingWithoutReplacement(randomGenerator, vHealthStatesList, N);
    std::vector<int> vNbIndivForEachHealthStates(vData.size(),0);
    for (int index = 0; index < vHealthStatesBuffer.size(); index++) {
        vNbIndivForEachHealthStates[vHealthStatesBuffer[index]] ++;
    }

    return vNbIndivForEachHealthStates;
}


std::vector<int> U_functions::ListAgeOfAnimals(const std::vector<int>& headcountByAge){
    std::vector<int> listOfAge;
    for (int i=0; i<headcountByAge.size(); i++) {
        for (int j=0; j<headcountByAge[i]; j++) {
            listOfAge.emplace_back(i);
        }
    }
    if (listOfAge.size()!=std::accumulate(headcountByAge.begin(), headcountByAge.end(), 0)) {
        std::cout << "Size error when creating the list of the age of the animals" << std::endl;
    }
    return listOfAge;
}


std::vector<int> U_functions::ListHealthStatesOfAnimals(const std::vector<int>& headcountByHealthStates){
    std::vector<int> listOfHealthStates;
    for (int i=0; i<headcountByHealthStates.size(); i++) {
        for (int j=0; j<headcountByHealthStates[i]; j++) {
            listOfHealthStates.emplace_back(i);
        }
    }
    if (listOfHealthStates.size()!=std::accumulate(headcountByHealthStates.begin(), headcountByHealthStates.end(), 0)) {
        std::cout << "Size error when creating the list of the age of the animals" << std::endl;
    }
    return listOfHealthStates;
}


std::vector<double> U_functions::getProportionsFromHeadcount(const std::vector<int>& headcount){
    std::vector<double> vProb(headcount.begin(), headcount.end());
    double tot = std::accumulate(headcount.begin(), headcount.end(), 0);
    for (int i=0; i<vProb.size(); i++) {
        vProb[i] = vProb[i]/static_cast<double>(tot);
    }
    return vProb;
}

std::vector<double> U_functions::getProportionsFromHeadcount(const std::vector<unsigned short int>& headcount){
    std::vector<double> vProb(headcount.begin(), headcount.end());
    double tot = std::accumulate(headcount.begin(), headcount.end(), 0);
    for (int i=0; i<vProb.size(); i++) {
        vProb[i] = vProb[i]/static_cast<double>(tot);
    }
    return vProb;
}


std::vector<double> U_functions::getZeroAndOneFromVector(const std::vector<unsigned short int>& vVector){
    std::vector<double> vZeroOne(vVector.begin(), vVector.end());
    for (int i = 0; i<vZeroOne.size(); i++) {
        vZeroOne[i] = (vZeroOne[i]>0)?1:0;
    }
    return vZeroOne;
}


std::vector<double> U_functions::VectorMultiplication(const std::vector<double>& vector1, const std::vector<double>& vector2){
    assert(vector1.size() == vector2.size());
    std::vector<double> vector3; vector3.resize(vector1.size(),0);
    for (int i = 0; i<vector1.size(); i++) {
        vector3[i] = vector1[i] * vector2[i];
    }
    return vector3;
}


double U_functions::gaussianPdf(const double& x, const double& mu, const double& sigma){
    double a = 1 / (sigma*sqrt(2*M_PI));
    double b = exp( -(std::pow(x-mu, 2)) / (2*std::pow(sigma, 2)) );
    
    double probaDensity = a * b;
    return probaDensity;
}







