//
//  ObservedSampleInformations.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 14/06/2016.
//  Copyright © 2016 Gaël Beaunée. All rights reserved.
//

#include "ObservedSampleInformations.hpp"

ObservedSampleInformations::ObservedSampleInformations(){}

ObservedSampleInformations::ObservedSampleInformations(const int& theObservedHeadount, const int& theObservedSamplesCount, const double& theObservedPositiveSamplesCount){
    observedHeadount = theObservedHeadount;
    observedSamplesCount = theObservedSamplesCount;
    observedPositiveSamplesCount = theObservedPositiveSamplesCount;
    
    proportionOfSample = std::min(static_cast<double>(theObservedSamplesCount) / static_cast<double>(observedHeadount), 1.0);
    proportionOfPositiveSample = static_cast<double>(theObservedPositiveSamplesCount) / static_cast<double>(theObservedSamplesCount);
}

