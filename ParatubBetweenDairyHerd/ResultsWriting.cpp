//
//  ResultsWriting.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 25/07/2017.
//  Copyright © 2017 Gaël Beaunée. All rights reserved.
//

#include "ResultsWriting.h"


//void ResultsWriting::SaveResults(PersistentResults& acc_persistentResults, GlobalResults& acc_globalResults){
//#ifndef ABCreduce
//    if (Parameters::ABCrejection == false & Parameters::ABCsmc == false & Parameters::ABCcompleteTrajectories == false) {
//        openFileAndWrite("vFarmID", Parameters::vFarmID);
//
//        openFileAndWrite("accMetapopPersistence", acc_globalResults.accMetapopPersistence);
//
//        openFileAndWrite("accNbHerdsWithIc", acc_persistentResults.accNbHerdsWithIc);
//        openFileAndWrite("accNbHerdsWithMoreThanTwoIc", acc_persistentResults.accNbHerdsWithMoreThanTwoIc);
//
//        openFileAndWrite("vInfectedHerdsVariation", acc_persistentResults.vInfectedHerdsVariation);
//        openFileAndWrite("vAlreadyInfectedHerdsVariation", acc_persistentResults.vAlreadyInfectedHerdsVariation);
//
//
//        openFileAndWrite("vOutDegreeOfSelectedNode", acc_persistentResults.vOutDegreeOfSelectedNode);
//        openFileAndWrite("vNbOutMovsOfSelectedNode", acc_persistentResults.vNbOutMovsOfSelectedNode);
//        openFileAndWrite("vInDegreeOfSelectedNode", acc_persistentResults.vInDegreeOfSelectedNode);
//        openFileAndWrite("vNbInMovsOfSelectedNode", acc_persistentResults.vNbInMovsOfSelectedNode);
//        openFileAndWrite("vSizeOfSelectedNode", acc_persistentResults.vSizeOfSelectedNode);
//        openFileAndWrite("vPrevalenceInSelectedNodes", acc_persistentResults.vPrevalenceInSelectedNodes);
//
//        openFileAndWrite("vNbNewInfectedHerdsSinceSimulationStart", acc_persistentResults.vNbNewInfectedHerdsSinceSimulationStart);
//        openFileAndWrite("vNbNewInfectedHerdsSinceTestsStart", acc_persistentResults.vNbNewInfectedHerdsSinceTestsStart);
//
//        openFileAndWrite("accInfectedHerds", acc_persistentResults.accInfectedHerds);
//        openFileAndWrite("accAffectedHerds", acc_persistentResults.accAffectedHerds);
//        openFileAndWrite("accNeverInfectedHerds", acc_persistentResults.accNeverInfectedHerds);
//        openFileAndWrite("accAlreadyInfectedHerds", acc_persistentResults.accAlreadyInfectedHerds);
//
//        openFileAndWrite("accNbInitiallyInfectedHerds", acc_persistentResults.accNbInitiallyInfectedHerds);
//        openFileAndWrite("accNbOtherInfectedHerds", acc_persistentResults.accNbOtherInfectedHerds);
//
//        openFileAndWrite("accInfectedProportionMeta", acc_persistentResults.accInfectedProportionMeta);
//        openFileAndWrite("accInfectiousProportionMeta", acc_persistentResults.accInfectiousProportionMeta);
//        openFileAndWrite("accAffectedProportionMeta", acc_persistentResults.accAffectedProportionMeta);
//
//        openFileAndWrite("accInfectedProportionFirst", acc_persistentResults.accInfectedProportionFirst);
//        openFileAndWrite("accInfectiousProportionFirst", acc_persistentResults.accInfectiousProportionFirst);
//        openFileAndWrite("accAffectedProportionFirst", acc_persistentResults.accAffectedProportionFirst);
//
//        //    if (Parameters::withinHerdBiosecurity == false) {
//        //        openFileAndWrite("accInfectedProportionFirstInit", acc_persistentResults.accInfectedProportionFirstInit);
//        //        openFileAndWrite("accInfectiousProportionFirstInit", acc_persistentResults.accInfectiousProportionFirstInit);
//        //        openFileAndWrite("accAffectedProportionFirstInit", acc_persistentResults.accAffectedProportionFirstInit);
//        //    }
//
//        openFileAndWrite("accInfectedProportionSecond", acc_persistentResults.accInfectedProportionSecond);
//        openFileAndWrite("accInfectiousProportionSecond", acc_persistentResults.accInfectiousProportionSecond);
//        openFileAndWrite("accAffectedProportionSecond", acc_persistentResults.accAffectedProportionSecond);
//
//
//        openFileAndWrite("accInfectedByFirst", acc_persistentResults.accInfectedByFirst);
//        openFileAndWrite("accInfectedBySecond", acc_persistentResults.accInfectedBySecond);
//        openFileAndWrite("accInfectedByExt", acc_persistentResults.accInfectedByExt);
//
//        openFileAndWrite("accInfectedByTestedMov", acc_persistentResults.accInfectedByTestedMov);
//
//        openFileAndWrite("AgeFailure", vAgeFailure);
//        openFileAndWrite("AgeMov", vAgeMov);
//        openFileAndWrite("AgeMovAfterCorrection", vAgeMovAfterCorrection);
//        //    openFileAndWrite("DateOfFirstCase", acc_globalResults.vDateOfFirstCase);
//
//
//
//        openFileAndWrite("accProportionOfOutgoingMovTested", acc_persistentResults.accProportionOfOutgoingMovTested);
//        openFileAndWrite("accProportionOfOutgoingInfectedMovTested", acc_persistentResults.accProportionOfOutgoingInfectedMovTested);
//
//        openFileAndWrite("accProportionOfIncomingMovTested", acc_persistentResults.accProportionOfIncomingMovTested);
//        openFileAndWrite("accProportionOfIncomingInfectedMovTested", acc_persistentResults.accProportionOfIncomingInfectedMovTested);
//
//
//        openFileAndWrite("vNumberOfOutgoingMovTested", acc_persistentResults.vNumberOfOutgoingMovTested);
//        openFileAndWrite("vNumberOfOutgoingMov", acc_persistentResults.vNumberOfOutgoingMov);
//
//        openFileAndWrite("vNumberOfIncomingMovTested", acc_persistentResults.vNumberOfIncomingMovTested);
//        openFileAndWrite("vNumberOfIncomingMov", acc_persistentResults.vNumberOfIncomingMov);
//
//
//        openFileAndWrite("vIntroducionOfInfectiousAnimalsIfNoTest", acc_persistentResults.vIntroducionOfInfectiousAnimalsIfNoTest);
//        openFileAndWrite("vIntroducionOfInfectiousAnimalsAvoided", acc_persistentResults.vIntroducionOfInfectiousAnimalsAvoided);
//
//        openFileAndWrite("vProportionOfIntroducionOfInfectiousAnimalsAvoided", acc_persistentResults.vProportionOfIntroducionOfInfectiousAnimalsAvoided);
//
//
//        openFileAndWrite("accInfectedHerdsQuantiles", acc_persistentResults.accInfectedHerdsQuantiles);
//        openFileAndWrite("accAlreadyInfectedHerdsQuantiles", acc_persistentResults.accAlreadyInfectedHerdsQuantiles);
//        openFileAndWrite("accAffectedHerdsQuantiles", acc_persistentResults.accAffectedHerdsQuantiles);
//
//
//        openFileAndWrite("accInfectedProportionMetaQuantiles", acc_persistentResults.accInfectedProportionMetaQuantiles);
//        openFileAndWrite("accInfectiousProportionMetaQuantiles", acc_persistentResults.accInfectiousProportionMetaQuantiles);
//        openFileAndWrite("accAffectedProportionMetaQuantiles", acc_persistentResults.accAffectedProportionMetaQuantiles);
//
//
//        openFileAndWrite("vPrevalenceInInfectedHerdsAtTheEnd", acc_persistentResults.vPrevalenceInInfectedHerdsAtTheEnd);
//        openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvPrevalenceInInfectedHerdsAtTheEnd", acc_persistentResults.vvPrevalenceInInfectedHerdsAtTheEnd);
//
//
//        if (Parameters::withinHerdBiosecurity == false) {
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vInfectionDurationWhenExtinction", acc_persistentResults.vInfectionDurationWhenExtinction);
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vInfectionDurationIfNoExtinction", acc_persistentResults.vInfectionDurationIfNoExtinction);
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vExtinctionBefore1Years", acc_persistentResults.vExtinctionBefore1Years);
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vExtinctionBefore2Years", acc_persistentResults.vExtinctionBefore2Years);
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vExtinctionBefore3Years", acc_persistentResults.vExtinctionBefore3Years);
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vExtinctionBefore5Years", acc_persistentResults.vExtinctionBefore5Years);
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vNoExtinctionBefore5Years", acc_persistentResults.vNoExtinctionBefore5Years);
//        }
//
//
//
//        //    if (Parameters::withinHerdBiosecurity == false) {
//        //        openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvDynamicHerdPrevalenceInfectedAfter5Years", acc_persistentResults.vvDynamicHerdPrevalenceInfectedAfter5Years);
//        //        openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvDynamicHerdPrevalenceInfectiousAfter5Years", acc_persistentResults.vvDynamicHerdPrevalenceInfectiousAfter5Years);
//        //        openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvDynamicHerdPrevalenceAffectedAfter5Years", acc_persistentResults.vvDynamicHerdPrevalenceAffectedAfter5Years);
//
//        //        openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvPersistenceDynamicOver5Years", acc_persistentResults.vvPersistenceDynamicOver5Years);
//        //    }
//
//        openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvInfosHerdPrevalenceAfter5Years", acc_persistentResults.vvInfosHerdPrevalenceAfter5Years);
//
//        openFileAndWriteMatrix_FirstIndexThenSecondIndex("vvPersistenceOver5Years", acc_persistentResults.vvPersistenceOver5Years);
//
//        if (Parameters::withinHerdBiosecurity == false) {
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vSourceAndDestinationOfInfectedMovements_SourceIsFirst", acc_persistentResults.vSourceAndDestinationOfInfectedMovements_SourceIsFirst);
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vSourceAndDestinationOfInfectedMovements_SourceIsSecond", acc_persistentResults.vSourceAndDestinationOfInfectedMovements_SourceIsSecond);
//            openFileAndWriteMatrix_FirstIndexThenSecondIndex("vSourceAndDestinationOfInfectedMovements_SourceIsExt", acc_persistentResults.vSourceAndDestinationOfInfectedMovements_SourceIsExt);
//        }
//
//        openFileAndWriteMatrix_FirstIndexThenSecondIndex("vNbInfectedMovementsPerHerd", acc_persistentResults.vNbInfectedMovementsPerHerd);
//
//
//        openFileAndWrite("vNumberOfTSaleByPrimary", acc_persistentResults.vNumberOfTSaleByPrimary);
//        openFileAndWrite("vNumberOfLSaleByPrimary", acc_persistentResults.vNumberOfLSaleByPrimary);
//        openFileAndWrite("vNumberOfIsSaleByPrimary", acc_persistentResults.vNumberOfIsSaleByPrimary);
//        openFileAndWrite("vNumberOfNewInfectionFromPrimary", acc_persistentResults.vNumberOfNewInfectionFromPrimary);
//
//        if (Parameters::withinHerdBiosecurity == false) {
//            //openFileAndWrite("vAccHeadcounts", acc_globalResults.vAccHeadcounts);
//            openFileAndWrite("vAccAnnualHeadcounts", acc_globalResults.vAccAnnualHeadcounts);
//            openFileAndWrite("vAccAnnualHeadcountsLact", acc_globalResults.vAccAnnualHeadcountsLact);
//        }
//        openFileAndWrite("vAccNbPres", acc_globalResults.vAccNbPres);
//        openFileAndWrite("vAccNbPresLact", acc_globalResults.vAccNbPresLact);
//        //        if (Parameters::withinHerdBiosecurity == false) {
//        //            openFileAndWrite("vAccAnnualBirths", acc_globalResults.vAccAnnualBirths);
//        //            openFileAndWrite("vAccAnnualBirthsPerAgeGroup", acc_globalResults.vAccAnnualBirthsPerAgeGroup);
//        //            openFileAndWrite("vAccBirthsPerAgeGroup", acc_globalResults.vAccBirthsPerAgeGroup);
//        //        }
//
//        if (nbPersistentSimu != 0) {
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfInfectedHerdsAfterHalfTheSimulationTime", acc_persistentResults.vvMatrixOfInfectedHerdsAfterHalfTheSimulationTime);
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime", acc_persistentResults.vvMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime);
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime", acc_persistentResults.vvMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime);
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime", acc_persistentResults.vvMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime);
//
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfInfection", acc_persistentResults.vvMatrixOfNumberOfInfection);
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfInfectedAnimalsPurchased", acc_persistentResults.vvMatrixOfNumberOfInfectedAnimalsPurchased);
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfWeeksWithInfection", acc_persistentResults.vvMatrixOfNumberOfWeeksWithInfection);
//
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvPrimaryCaseMatrix", acc_persistentResults.vvPrimaryCaseMatrix);
//
//
//            //            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfAnimalsTestedMovIn", acc_persistentResults.vvMatrixOfNumberOfAnimalsTestedMovIn);
//            //            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfAnimalsTestedMovOut", acc_persistentResults.vvMatrixOfNumberOfAnimalsTestedMovOut);
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfIcAnimalsCulled", acc_persistentResults.vvMatrixOfNumberOfIcAnimalsCulled);
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfAnimalsCulledDuringTestAndCull", acc_persistentResults.vvMatrixOfNumberOfAnimalsCulledDuringTestAndCull);
//            openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID("vvMatrixOfNumberOfAnimalsTestedDuringTestAndCull", acc_persistentResults.vvMatrixOfNumberOfAnimalsTestedDuringTestAndCull);
//        }
//
//
//
//        openFileAndWrite("vMedianInfectedHerds", acc_persistentResults.vMedianInfectedHerds);
//        openFileAndWrite("vInfectedHerds", acc_persistentResults.vInfectedHerds);
//        openFileAndWrite("vAlreadyInfectedHerds", acc_persistentResults.vAlreadyInfectedHerds);
//        openFileAndWrite("vAffectedHerds", acc_persistentResults.vAffectedHerds);
//
//        openFileAndWrite("vInfectedProportionMeta", acc_persistentResults.vInfectedProportionMeta);
//        openFileAndWrite("vInfectiousProportionMeta", acc_persistentResults.vInfectiousProportionMeta);
//        openFileAndWrite("vAffectedProportionMeta", acc_persistentResults.vAffectedProportionMeta);
//
//        openFileAndWrite("accNbHerdWithTestOfMovements", acc_persistentResults.accNbHerdWithTestOfMovements);
//        openFileAndWrite("accNbHerdWithCullingImprovement", acc_persistentResults.accNbHerdWithCullingImprovement);
//        openFileAndWrite("accNbHerdWithHygieneImprovement", acc_persistentResults.accNbHerdWithHygieneImprovement);
//        openFileAndWrite("accNbHerdWithCalfManagementImprovement", acc_persistentResults.accNbHerdWithCalfManagementImprovement);
//        openFileAndWrite("accNbHerdWithTestAndCull", acc_persistentResults.accNbHerdWithTestAndCull);
//
//        saveIntraHerdPrevalenceAtTheEnd();
//    }
//#endif
//
//    if (Parameters::ABCrejection == true) {
//        iABCInfosAndResults.saveABCrejectionResults();
//    }
//
//    if (Parameters::ABCsmc == true) {
//        std::string fileName = "ABCsmcSequenceCompleted_" + std::to_string(Parameters::ABCsmcSequenceNumber);
//        std::ofstream ost_ABCsmcCompleted(Parameters::ABCfolderPath + "ABCsmcSequencesCompleted/" + fileName + ".csv", std::fstream::out);
//        if(!ost_ABCsmcCompleted){std::cerr << "[ERROR] can't open output file : " + fileName + ".csv" << std::endl;} // Check if the file is open!
//        // Writing ...
//        ost_ABCsmcCompleted << "ABCsmc sequence " + std::to_string(Parameters::ABCsmcSequenceNumber) + " is completed!" << "\n";
//    }
//}



void ResultsWriting::openFileAndWrite(std::string fileName, const accVectDouble& AccumulatorsVector){
    // MEAN
    std::ofstream ost1(Parameters::ResultsFolderPath + fileName +"_mean.csv", std::fstream::out); // Open file in writing mode
    if(!ost1){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    std::vector<double> vMean = mean(AccumulatorsVector);
    for (int i=0; i<vMean.size(); i++) { ost1 << std::fixed << vMean[i] << "\t"; } // std::scientific // setprecision(n)
    ost1 << "\n";

    // VARIANCE
    std::ofstream ost2(Parameters::ResultsFolderPath + fileName +"_variance.csv", std::fstream::out); // Open file in writing mode
    if(!ost2){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    std::vector<double> vVariance = variance(AccumulatorsVector);
    for (int i=0; i<vVariance.size(); i++) { ost2 << std::fixed << vVariance[i] << "\t"; } // std::scientific // setprecision(n)
    ost2 << "\n";

    //    // COUNT
    //    std::ofstream ost3(Parameters::ResultsFolderPath + fileName +"_count.csv", std::fstream::out);
    //    if(!ost3){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    //    // Writing ...
    //    int rCount = static_cast<int>(count(AccumulatorsVector));
    //    ost3 << std::fixed << rCount << "\t"; // std::scientific // setprecision(n)
    //    ost3 << "\n";
    //
    //    // SUM
    //    std::ofstream ost4(Parameters::ResultsFolderPath + fileName +"_sum.csv", std::fstream::out);
    //    if(!ost4){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    //    // Writing ...
    //    std::vector<double> vSum = sum(AccumulatorsVector);
    //    for (int i=0; i<vSum.size(); i++) { ost4 << std::fixed << vSum[i] << "\t"; } // std::scientific // setprecision(n)
    //    ost4 << "\n";

}


void ResultsWriting::openFileAndWrite(std::string fileName, const accVectInt& AccumulatorsVector){
    // MEAN
    std::ofstream ost1(Parameters::ResultsFolderPath + fileName +"_mean.csv", std::fstream::out); // Open file in writing mode
    if(!ost1){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    std::vector<double> vMean = mean(AccumulatorsVector);
    for (int i=0; i<vMean.size(); i++) { ost1 << std::fixed << vMean[i] << "\t"; } // std::scientific // setprecision(n)
    ost1 << "\n";

    // VARIANCE
    std::ofstream ost2(Parameters::ResultsFolderPath + fileName +"_variance.csv", std::fstream::out); // Open file in writing mode
    if(!ost2){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    std::vector<double> vVariance = variance(AccumulatorsVector);
    for (int i=0; i<vVariance.size(); i++) { ost2 << std::fixed << vVariance[i] << "\t"; } // std::scientific // setprecision(n)
    ost2 << "\n";

    //    // COUNT
    //    std::ofstream ost3(Parameters::ResultsFolderPath + fileName +"_count.csv", std::fstream::out);
    //    if(!ost3){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    //    // Writing ...
    //    int rCount = static_cast<int>(count(AccumulatorsVector));
    //    ost3 << std::fixed << rCount << "\t"; // std::scientific // setprecision(n)
    //    ost3 << "\n";
    //
    //    // SUM
    //    std::ofstream ost4(Parameters::ResultsFolderPath + fileName +"_sum.csv", std::fstream::out);
    //    if(!ost4){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    //    // Writing ...
    //    std::vector<int> vSum = sum(AccumulatorsVector);
    //    for (int i=0; i<vSum.size(); i++) { ost4 << std::fixed << vSum[i] << "\t"; } // std::scientific // setprecision(n)
    //    ost4 << "\n";

}


void ResultsWriting::openFileAndWrite(std::string fileName, const std::vector<double>& Results){
    // Open file in writing mode
    std::ofstream ost(Parameters::ResultsFolderPath + fileName +".csv", std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int i=0; i<Results.size(); i++) { ost << std::fixed << Results[i] << "\t"; } // std::scientific // setprecision(n)
    ost << "\n";
}


void ResultsWriting::openFileAndWrite(std::string fileName, const std::vector<int>& Results){
    // Open file in writing mode
    std::ofstream ost(Parameters::ResultsFolderPath + fileName +".csv", std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int i=0; i<Results.size(); i++) { ost << std::fixed << Results[i] << "\t"; } // std::scientific // setprecision(n)
    ost << "\n";
}


void ResultsWriting::openFileAndWrite(std::string fileName, const std::vector<std::string>& Results){
    // Open file in writing mode
    std::ofstream ost(Parameters::ResultsFolderPath + fileName +".csv", std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int i=0; i<Results.size(); i++) { ost << Results[i] << "\t"; } // std::scientific // setprecision(n)
    ost << "\n";
}


void ResultsWriting::openFileAndWrite(std::string fileName, const accInt& AccumulatorsInt){
    // MEAN
    std::ofstream ost1(Parameters::ResultsFolderPath + fileName +"_mean.csv", std::fstream::out); // Open file in writing mode
    if(!ost1){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    double bMean = mean(AccumulatorsInt);
    ost1 << std::fixed << bMean << "\t"; // std::scientific // setprecision(n)
    ost1 << "\n";

    // VARIANCE
    std::ofstream ost2(Parameters::ResultsFolderPath + fileName +"_variance.csv", std::fstream::out); // Open file in writing mode
    if(!ost2){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    double bVariance = variance(AccumulatorsInt);
    ost2 << std::fixed << bVariance << "\t"; // std::scientific // setprecision(n)
    ost2 << "\n";

    //    // COUNT
    //    std::ofstream ost3(Parameters::ResultsFolderPath + fileName +"_count.csv", std::fstream::out);
    //    if(!ost3){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    //    // Writing ...
    //    int bCount = static_cast<int>(count(AccumulatorsInt));
    //    ost3 << std::fixed << bCount << "\t"; // std::scientific // setprecision(n)
    //    ost3 << "\n";
    //
    //    // SUM
    //    std::ofstream ost4(Parameters::ResultsFolderPath + fileName +"_sum.csv", std::fstream::out);
    //    if(!ost4){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    //    // Writing ...
    //    double bSum = sum(AccumulatorsInt);
    //    ost4 << std::fixed << bSum << "\t"; // std::scientific // setprecision(n)
    //    ost4 << "\n";

}


void ResultsWriting::openFileAndWrite(std::string fileName, const vectAccDouble& VectorAccumulators){
    // MEAN
    std::ofstream ost1(Parameters::ResultsFolderPath + fileName +"_mean.csv", std::fstream::out);
    if(!ost1){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int time = 0; time < VectorAccumulators.size(); time++) {ost1 << std::fixed << mean(VectorAccumulators[time]) << "\t";} // std::scientific // setprecision(n)
    ost1 << "\n";

    // VARIANCE
    std::ofstream ost2(Parameters::ResultsFolderPath + fileName +"_variance.csv", std::fstream::out);
    if(!ost2){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int time = 0; time < VectorAccumulators.size(); time++) {ost2 << std::fixed << variance(VectorAccumulators[time]) << "\t";} // std::scientific // setprecision(n)
    ost2 << "\n";

    //    // COUNT
    //    std::ofstream ost3(Parameters::ResultsFolderPath + fileName +"_count.csv", std::fstream::out);
    //    if(!ost3){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    //    // Writing ...
    //    for (int time = 0; time < VectorAccumulators.size(); time++) {ost3 << std::fixed << static_cast<int>(count(VectorAccumulators[time])) << "\t";} // std::scientific // setprecision(n)
    //    ost3 << "\n";
    //
    //    // SUM
    //    std::ofstream ost4(Parameters::ResultsFolderPath + fileName +"_sum.csv", std::fstream::out);
    //    if(!ost4){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    //    // Writing ...
    //    for (int time = 0; time < VectorAccumulators.size(); time++) {ost4 << std::fixed << sum(VectorAccumulators[time]) << "\t";} // std::scientific // setprecision(n)
    //    ost4 << "\n";

}


void ResultsWriting::openFileAndWrite(std::string fileName, const accVectQuantiles& AccumulatorsQuantiles){
    // quantile 01
    std::ofstream ost1(Parameters::ResultsFolderPath + fileName +"_01.csv", std::fstream::out);
    if(!ost1){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!


    // quantile 025
    std::ofstream ost2(Parameters::ResultsFolderPath + fileName +"_025.csv", std::fstream::out);
    if(!ost2){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!


    // quantile 05
    std::ofstream ost3(Parameters::ResultsFolderPath + fileName +"_05.csv", std::fstream::out);
    if(!ost3){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!


    // quantile 05
    std::ofstream ost4(Parameters::ResultsFolderPath + fileName +"_10.csv", std::fstream::out);
    if(!ost4){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!


    // quantile 95
    std::ofstream ost5(Parameters::ResultsFolderPath + fileName +"_90.csv", std::fstream::out);
    if(!ost5){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!


    // quantile 95
    std::ofstream ost6(Parameters::ResultsFolderPath + fileName +"_95.csv", std::fstream::out);
    if(!ost6){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!


    // quantile 975
    std::ofstream ost7(Parameters::ResultsFolderPath + fileName +"_975.csv", std::fstream::out);
    if(!ost7){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!


    // quantile 99
    std::ofstream ost8(Parameters::ResultsFolderPath + fileName +"_99.csv", std::fstream::out);
    if(!ost8){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!


    // quantile median
    std::ofstream ost9(Parameters::ResultsFolderPath + fileName +"_median.csv", std::fstream::out);
    if(!ost9){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!



    // Writing ...
    for (int time = 0; time < AccumulatorsQuantiles.size(); time++) {
        ost1 << std::fixed << extended_p_square(AccumulatorsQuantiles[time])[0] << "\t";
        ost2 << std::fixed << extended_p_square(AccumulatorsQuantiles[time])[1] << "\t";
        ost3 << std::fixed << extended_p_square(AccumulatorsQuantiles[time])[2] << "\t";
        ost4 << std::fixed << extended_p_square(AccumulatorsQuantiles[time])[3] << "\t";
        ost5 << std::fixed << extended_p_square(AccumulatorsQuantiles[time])[5] << "\t";
        ost6 << std::fixed << extended_p_square(AccumulatorsQuantiles[time])[6] << "\t";
        ost7 << std::fixed << extended_p_square(AccumulatorsQuantiles[time])[7] << "\t";
        ost8 << std::fixed << extended_p_square(AccumulatorsQuantiles[time])[8] << "\t";
        ost9 << std::fixed << extended_p_square(AccumulatorsQuantiles[time])[4] << "\t";
    }
    ost1 << "\n";
    ost2 << "\n";
    ost3 << "\n";
    ost4 << "\n";
    ost5 << "\n";
    ost6 << "\n";
    ost7 << "\n";
    ost8 << "\n";
    ost9 << "\n";


}


void ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex(std::string fileName, std::vector<std::vector<double>>& Results){
    // Open file in writing mode
    std::ofstream ost(Parameters::ResultsFolderPath + fileName +".csv", std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int rep=0; rep<Results.size(); rep++) {
        for (int herd=0; herd<Results[rep].size(); herd++) {
            if (Results[rep][herd] == floor(Results[rep][herd])) {
                ost << Results[rep][herd] << "\t";
            } else {
                ost << Results[rep][herd] << "\t";
            }
        }
        ost << "\n";
    } // std::scientific // setprecision(n)
}


void ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex(std::string fileName, std::vector<std::vector<int>>& Results){
    // Open file in writing mode
    std::ofstream ost(Parameters::ResultsFolderPath + fileName +".csv", std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int rep=0; rep<Results.size(); rep++) {
        for (int herd=0; herd<Results[rep].size(); herd++) {
            if (Results[rep][herd] == floor(Results[rep][herd])) {
                ost << Results[rep][herd] << "\t";
            } else {
                ost << Results[rep][herd] << "\t";
            }
        }
        ost << "\n";
    } // std::scientific // setprecision(n)
}


void ResultsWriting::openFileAndWriteMatrix_FirstIndexThenSecondIndex(std::string fileName, std::vector<std::vector<std::string>>& Results){
    // Open file in writing mode
    std::ofstream ost(Parameters::ResultsFolderPath + fileName +".csv", std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int rep=0; rep<Results.size(); rep++) {
        for (int herd=0; herd<Results[rep].size(); herd++) {
            if (Results[rep][herd] == Results[rep][herd]) {
                ost << Results[rep][herd] << "\t";
            } else {
                ost << Results[rep][herd] << "\t";
            }
        }
        ost << "\n";
    } // std::scientific // setprecision(n)
}


void ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndex(std::string fileName, std::vector<std::vector<int>>& Results){
    // Open file in writing mode
    std::ofstream ost(Parameters::ResultsFolderPath + fileName +".csv", std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int herd=0; herd<Results[0].size(); herd++) {
        for (int rep=0; rep<Results.size(); rep++) {
            ost << std::fixed << Results[rep][herd] << "\t";
        }
        ost << "\n";
    } // std::scientific // setprecision(n)
}


void ResultsWriting::openFileAndWriteMatrix_SecondIndexThenFirstIndexWithID(std::string fileName, std::vector<std::vector<int>>& Results){
    // Open file in writing mode
    std::ofstream ost(Parameters::ResultsFolderPath + fileName +".csv", std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int herd=0; herd<Results[0].size(); herd++) {
        ost << Parameters::vFarmID[herd] << "\t";
        for (int rep=0; rep<Results.size(); rep++) {
            ost << std::fixed << Results[rep][herd] << "\t";
        }
        ost << "\n";
    } // std::scientific // setprecision(n)
}


void ResultsWriting::openFileAndWrite(std::string fileName, const std::vector<accVectInt>& vectAccVectInt){
    // MEAN
    std::ofstream ost1(Parameters::ResultsFolderPath + fileName +"_mean.csv", std::fstream::out); // Open file in writing mode
    if(!ost1){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int herd=0; herd<vectAccVectInt.size(); herd++) {
        std::vector<double> vMean = mean(vectAccVectInt[herd]);
        for (int i=0; i<vMean.size(); i++) { ost1 << std::fixed << vMean[i] << "\t"; } // std::scientific // setprecision(n)
        ost1 << "\n";
    }

    // VARIANCE
    std::ofstream ost2(Parameters::ResultsFolderPath + fileName +"_variance.csv", std::fstream::out); // Open file in writing mode
    if(!ost2){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int herd=0; herd<vectAccVectInt.size(); herd++) {
        std::vector<double> vVariance = variance(vectAccVectInt[herd]);
        for (int i=0; i<vVariance.size(); i++) { ost2 << std::fixed << vVariance[i] << "\t"; } // std::scientific // setprecision(n)
        ost2 << "\n";
    }

}


void ResultsWriting::openFileAndWrite(std::string fileName, const std::vector<accVectDouble>& vectAccVectDouble){
    // MEAN
    std::ofstream ost1(Parameters::ResultsFolderPath + fileName +"_mean.csv", std::fstream::out); // Open file in writing mode
    if(!ost1){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int herd=0; herd<vectAccVectDouble.size(); herd++) {
        std::vector<double> vMean = mean(vectAccVectDouble[herd]);
        for (int i=0; i<vMean.size(); i++) { ost1 << std::fixed << vMean[i] << "\t"; } // std::scientific // setprecision(n)
        ost1 << "\n";
    }

    // VARIANCE
    std::ofstream ost2(Parameters::ResultsFolderPath + fileName +"_variance.csv", std::fstream::out); // Open file in writing mode
    if(!ost2){std::cerr << "[ERROR] can't open output file : " + fileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int herd=0; herd<vectAccVectDouble.size(); herd++) {
        std::vector<double> vVariance = variance(vectAccVectDouble[herd]);
        for (int i=0; i<vVariance.size(); i++) { ost2 << std::fixed << vVariance[i] << "\t"; } // std::scientific // setprecision(n)
        ost2 << "\n";
    }

}


//void ResultsWriting::saveInfectionDynamic(int run, std::map<std::string, DairyHerd>& mFarms){
//    if (run < 5) {
//        std::ofstream ost(Parameters::ResultsFolderPath + "infectionDynamic/dynamic_" + std::to_string(run) +".csv", std::fstream::out);
//        if(!ost){std::cerr << "[ERROR] can't open output file : infectionDynamic" << std::endl;}
//
//        for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
//            for (int i = 0; i < it->second.vInfectionDynamic.size(); i++) {
//                ost << it->second.vInfectionDynamic[i] << "\t";
//            }
//            ost << "\n";
//        }
//    }
//
//}


//void ResultsWriting::updateAccSecondPrev(std::map<std::string, DairyHerd>& mFarms){
//    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
//        if (it->second.typeInfectedHerd == 2) {
//            if (it->second.persistenceIndicator) {
//                if (it->second.numberOfWeeksSinceInfected>(Parameters::simutime-Parameters::timestepInOneYear)) {
//                    for (int i = 0; i<it->second.numberOfWeeksSinceInfected; i++) {
//                        double bInfectedSecond = it->second.iSummaryDynamic.infected[Parameters::simutime-1-it->second.numberOfWeeksSinceInfected+i];
//                        double bInfectiousSecond = it->second.iSummaryDynamic.infectious[Parameters::simutime-1-it->second.numberOfWeeksSinceInfected+i];
//                        double bAffectedSecond = it->second.iSummaryDynamic.affected[Parameters::simutime-1-it->second.numberOfWeeksSinceInfected+i];
//                        double bHeadcountSecond = it->second.iSummaryDynamic.headcount[Parameters::simutime-1-it->second.numberOfWeeksSinceInfected+i];
//
//                        acc_persistentResults.accInfectedProportionSecond[i](bInfectedSecond/bHeadcountSecond);
//                        acc_persistentResults.accInfectiousProportionSecond[i](bInfectiousSecond/bHeadcountSecond);
//                        acc_persistentResults.accAffectedProportionSecond[i](bAffectedSecond/bHeadcountSecond);
//                    }
//                }
//            }
//        }
//    }
//}


//void ResultsWriting::updateIntraHerdPrevalenceAtTheEnd(std::map<std::string, DairyHerd>& mFarms){
//    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
//        double prev = static_cast<double>(it->second.iSummaryDynamic.infected[Parameters::simutime-1])/static_cast<double>(it->second.iSummaryDynamic.headcount[Parameters::simutime-1]);
//        vvIntraHerdPrevalenceAtTheEnd[it->first].emplace_back(prev);
//    }
//}

//void ResultsWriting::saveIntraHerdPrevalenceAtTheEnd(){
//    std::ofstream ost(Parameters::ResultsFolderPath + "vvIntraHerdPrevalenceAtTheEnd.csv", std::fstream::out);
//    if(!ost){std::cerr << "[ERROR] can't open output file : vvIntraHerdPrevalenceAtTheEnd.csv" << std::endl;}
//
//    ost << "herd";
//    for (int i = 1; i<=Parameters::nbRuns; i++) {
//        ost << "\t" << "prev_" << i;
//    }
//    ost << "\n";
//
//    for (auto it = vvIntraHerdPrevalenceAtTheEnd.begin(); it != vvIntraHerdPrevalenceAtTheEnd.end(); ++it) {
//        ost << it->first << "\t";
//        for (int i = 0; i < it->second.size(); i++) {
//            if (i < it->second.size()-1) {
//                ost << it->second[i] << "\t";
//            } else {
//                ost << it->second[i];
//            }
//        }
//        ost << "\n";
//    }
//}



bool ResultsWriting::saveABCcompleteTrajectoriesResults(std::map<std::string, DairyHerd>& mFarms, const int& run){
    bool nanFound = false;

    int theCorrectedRunID = run+(Parameters::firstRunID-1)-Parameters::nbRunsWithNan;

    //
    for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
        if (it->second.iParametersSpecificToEachHerd.samplingDates.size() > 0) { // Parameters::mFarmsDateSampleCount.find(currentId) != Parameters::mFarmsDateSampleCount.end()
            for (int i = 0; i < it->second.vSummaryStatistics.size(); i++) {
                if (i < it->second.vSummaryStatistics.size()-1) {
                    if (it->second.vSummaryStatistics[i] != -1) {
                        if ( std::isnan(it->second.vSummaryStatistics[i]) ) {
                            nanFound = true;
                            //std::cout << "farmID: " << it->first << std::endl;
                        }
                    }
                } else {
                    if (it->second.vSummaryStatistics[i] != -1) {
                        if ( std::isnan(it->second.vSummaryStatistics[i]) ) {
                            nanFound = true;
                            //std::cout << "farmID: " << it->first << std::endl;
                        }
                    }
                }
            }
        }
    }
    //

    if (nanFound == false) {
        std::string fileName = "vvABCcompleteTrajectoriesSummaryStatistics_" + std::to_string(Parameters::ABCcompleteTrajectoriesSequenceNumber) + "_" + std::to_string(theCorrectedRunID) + ".csv";
        std::ofstream ost(Parameters::ResultsFolderPath + fileName, std::fstream::out);
        if(!ost){std::cerr << "[ERROR] can't open output file : " << fileName << std::endl;}

        for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
            if (it->second.iParametersSpecificToEachHerd.samplingDates.size() > 0) { // Parameters::mFarmsDateSampleCount.find(currentId) != Parameters::mFarmsDateSampleCount.end()
                ost << it->first << "\t";
                for (int i = 0; i < it->second.vSummaryStatistics.size(); i++) {
                    if (i < it->second.vSummaryStatistics.size()-1) {
                        if (it->second.vSummaryStatistics[i] != -1) {
                            ost << it->second.vSummaryStatistics[i] << "\t";
                        }
                    } else {
                        if (it->second.vSummaryStatistics[i] != -1) {
                            ost << it->second.vSummaryStatistics[i];
                        }
                    }
                }
                ost << "\n";
            }
        }

        std::string fileName_N = "vvABCcompleteTrajectoriesSummaryStatistics_N_" + std::to_string(Parameters::ABCcompleteTrajectoriesSequenceNumber) + "_" + std::to_string(theCorrectedRunID) + ".csv";
        std::ofstream ost_N(Parameters::ResultsFolderPath + fileName_N, std::fstream::out);
        if(!ost_N){std::cerr << "[ERROR] can't open output file : " << fileName_N << std::endl;}

        for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
            if (it->second.iParametersSpecificToEachHerd.samplingDates.size() > 0) {
                ost_N << it->first << "\t";
                for (int i = 0; i < it->second.vSummaryStatistics_N.size(); i++) {
                    if (i < it->second.vSummaryStatistics_N.size()-1) {
                        if (it->second.vSummaryStatistics_N[i] != -1) {
                            ost_N << it->second.vSummaryStatistics_N[i] << "\t";
                        }
                    } else {
                        if (it->second.vSummaryStatistics_N[i] != -1) {
                            ost_N << it->second.vSummaryStatistics_N[i];
                        }
                    }
                }
                ost_N << "\n";
            }
        }

        std::string fileName_N_pos = "vvABCcompleteTrajectoriesSummaryStatistics_N_pos_" + std::to_string(Parameters::ABCcompleteTrajectoriesSequenceNumber) + "_" + std::to_string(theCorrectedRunID) + ".csv";
        std::ofstream ost_N_pos(Parameters::ResultsFolderPath + fileName_N_pos, std::fstream::out);
        if(!ost_N_pos){std::cerr << "[ERROR] can't open output file : " << fileName_N_pos << std::endl;}

        for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
            if (it->second.iParametersSpecificToEachHerd.samplingDates.size() > 0) {
                ost_N_pos << it->first << "\t";
                for (int i = 0; i < it->second.vSummaryStatistics_N_pos.size(); i++) {
                    if (i < it->second.vSummaryStatistics_N_pos.size()-1) {
                        if (it->second.vSummaryStatistics_N_pos[i] != -1) {
                            ost_N_pos << it->second.vSummaryStatistics_N_pos[i] << "\t";
                        }
                    } else {
                        if (it->second.vSummaryStatistics_N_pos[i] != -1) {
                            ost_N_pos << it->second.vSummaryStatistics_N_pos[i];
                        }
                    }
                }
                ost_N_pos << "\n";
            }
        }

        std::string fileName_Nech = "vvABCcompleteTrajectoriesSummaryStatistics_Nech_" + std::to_string(Parameters::ABCcompleteTrajectoriesSequenceNumber) + "_" + std::to_string(theCorrectedRunID) + ".csv";
        std::ofstream ost_Nech(Parameters::ResultsFolderPath + fileName_Nech, std::fstream::out);
        if(!ost_Nech){std::cerr << "[ERROR] can't open output file : " << fileName_Nech << std::endl;}

        for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
            if (it->second.iParametersSpecificToEachHerd.samplingDates.size() > 0) {
                ost_Nech << it->first << "\t";
                for (int i = 0; i < it->second.vSummaryStatistics_Nech.size(); i++) {
                    if (i < it->second.vSummaryStatistics_Nech.size()-1) {
                        if (it->second.vSummaryStatistics_Nech[i] != -1) {
                            ost_Nech << it->second.vSummaryStatistics_Nech[i] << "\t";
                        }
                    } else {
                        if (it->second.vSummaryStatistics_Nech[i] != -1) {
                            ost_Nech << it->second.vSummaryStatistics_Nech[i];
                        }
                    }
                }
                ost_Nech << "\n";
            }
        }

        std::string fileName_Nech_pos = "vvABCcompleteTrajectoriesSummaryStatistics_Nech_pos_" + std::to_string(Parameters::ABCcompleteTrajectoriesSequenceNumber) + "_" + std::to_string(theCorrectedRunID) + ".csv";
        std::ofstream ost_Nech_pos(Parameters::ResultsFolderPath + fileName_Nech_pos, std::fstream::out);
        if(!ost_Nech_pos){std::cerr << "[ERROR] can't open output file : " << fileName_Nech_pos << std::endl;}

        for (auto it = mFarms.begin(); it != mFarms.end(); ++it) {
            if (it->second.iParametersSpecificToEachHerd.samplingDates.size() > 0) {
                ost_Nech_pos << it->first << "\t";
                for (int i = 0; i < it->second.vSummaryStatistics_Nech_pos.size(); i++) {
                    if (i < it->second.vSummaryStatistics_Nech_pos.size()-1) {
                        if (it->second.vSummaryStatistics_Nech_pos[i] != -1) {
                            ost_Nech_pos << it->second.vSummaryStatistics_Nech_pos[i] << "\t";
                        }
                    } else {
                        if (it->second.vSummaryStatistics_Nech_pos[i] != -1) {
                            ost_Nech_pos << it->second.vSummaryStatistics_Nech_pos[i];
                        }
                    }
                }
                ost_Nech_pos << "\n";
            }
        }
    }

    return nanFound;
}

void ResultsWriting::openFileAndWriteMatrix_farmsInLine_timeInColumn_forTheCurrentRun(std::string fileName, std::map<std::string, std::vector<double>>& Results, const int& run){
    int theCorrectedRunID = run+(Parameters::firstRunID-1)-Parameters::nbRunsWithNan;
    std::string completeFileName = fileName + "_" + std::to_string(theCorrectedRunID) + ".csv";
    std::ofstream ost(Parameters::ResultsFolderPath + completeFileName, std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " << completeFileName << std::endl;}

    for (auto farmID : Parameters::vFarmID){
        ost << farmID << ",";
        for (int i = 0; i < Results[farmID].size(); i++) {
            if (i < Results[farmID].size()-1) {
                ost << Results[farmID][i] << ",";
            } else {
                ost << Results[farmID][i];
            }
        }
        ost << "\n";
    }
}

void ResultsWriting::openFileAndWrite(std::string fileName, const std::vector<int>& Results, const int& run){
    // Open file in writing mode
    std::string completeFileName = fileName + "_" + std::to_string(run) + ".csv";
    std::ofstream ost(Parameters::ResultsFolderPath + completeFileName, std::fstream::out);
    if(!ost){std::cerr << "[ERROR] can't open output file : " + completeFileName << std::endl;} // Check if the file is open!
    // Writing ...
    for (int i=0; i<Results.size(); i++) {
        if (i < Results.size()-1) {
            ost << std::fixed << Results[i] << ",";
        } else {
            ost << std::fixed << Results[i];
        }
    }
    ost << "\n";
}

//void openFileAndWrite_mAcc(std::string fileName, std::map<std::string, accVectDouble>& mFarms_AccumulatorsVector){
//    // MEAN
//    std::string completeFileName_mean = fileName + "_mean.csv";
//    std::ofstream ost_mean(Parameters::ResultsFolderPath + completeFileName_mean, std::fstream::out);
//    if(!ost_mean){std::cerr << "[ERROR] can't open output file : " << completeFileName_mean << std::endl;}
//    // VARIANCE
//    std::string completeFileName_variance = fileName + "_variance.csv";
//    std::ofstream ost_variance(Parameters::ResultsFolderPath + completeFileName_variance, std::fstream::out);
//    if(!ost_variance){std::cerr << "[ERROR] can't open output file : " << completeFileName_variance << std::endl;}
//
//    for (auto farmID : Parameters::vFarmID){
//        ost_mean << farmID << ",";
//        ost_variance << farmID << ",";
//
//        // MEAN
//        std::vector<double> vMean = mean(mFarms_AccumulatorsVector[farmID]);
//        for (int i=0; i<vMean.size(); i++) {
//            if (i < vMean.size()-1) {
//                ost_mean << vMean[i] << ",";
//            } else {
//                ost_mean << vMean[i];
//            }
//        }
//        ost_mean << "\n";
//
//
//        // VARIANCE
//        std::vector<double> vVariance = variance(mFarms_AccumulatorsVector[farmID]);
//        for (int i=0; i<vVariance.size(); i++) {
//            if (i < vVariance.size()-1) {
//                ost_variance << vVariance[i] << ",";
//            } else {
//                ost_variance << vVariance[i];
//            }
//        }
//        ost_variance << "\n";
//    }
//}
