//
//  PersistentResults.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 09/07/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

//=================================================================
// Accumulator for runs which persist at the end of the simulation
//=================================================================

#include "PersistentResults.h"

// Constructor =========================================================
PersistentResults::PersistentResults(){
    // Vectors =============================================================
    
    // accumulators ========================================================
    accInfectedHerds = accVectInt(std::vector<int>(Parameters::simutime));
    accAffectedHerds = accVectInt(std::vector<int>(Parameters::simutime));
    accNeverInfectedHerds = accVectInt(std::vector<int>(Parameters::simutime));
    accAlreadyInfectedHerds = accVectInt(std::vector<int>(Parameters::simutime));
    
    accInfectedProportionMeta = accVectDouble(std::vector<double>(Parameters::simutime));
    accInfectiousProportionMeta = accVectDouble(std::vector<double>(Parameters::simutime));
    accAffectedProportionMeta = accVectDouble(std::vector<double>(Parameters::simutime));
    
    accInfectedProportionFirst = accVectDouble(std::vector<double>(Parameters::simutime));
    accInfectiousProportionFirst = accVectDouble(std::vector<double>(Parameters::simutime));
    accAffectedProportionFirst = accVectDouble(std::vector<double>(Parameters::simutime));
    
//    accInfectedProportionFirstInit = accVectDouble(std::vector<double>(Parameters::initializeSimutime));
//    accInfectiousProportionFirstInit = accVectDouble(std::vector<double>(Parameters::initializeSimutime));
//    accAffectedProportionFirstInit = accVectDouble(std::vector<double>(Parameters::initializeSimutime));
    
    accInfectedProportionSecond.resize(Parameters::simutime);
    accInfectiousProportionSecond.resize(Parameters::simutime);
    accAffectedProportionSecond.resize(Parameters::simutime);
    for (int time = 0; time < Parameters::simutime; time++) {
        accInfectedProportionSecond[time] = accDouble();
        accInfectiousProportionSecond[time] = accDouble();
        accAffectedProportionSecond[time] = accDouble();
    }
    
//    bHeadcountMeta.resize(Parameters::simutime, 0);
//    bHeadcountFirst.resize(Parameters::simutime, 0);
//    bHeadcountFirstInit.resize(Parameters::initializeSimutime, 0);
//    bHeadcountSecond.resize(Parameters::simutime, 0);
    
    
    
    accInfectedByFirst = accInt();
    accInfectedBySecond = accInt();
    accInfectedByExt = accInt();
    
    accInfectedByTestedMov = accInt();
    
    accNbHerdsWithIc = accVectInt(std::vector<int>(Parameters::simutime));
    accNbHerdsWithMoreThanTwoIc = accVectInt(std::vector<int>(Parameters::simutime));
    
    accNbInitiallyInfectedHerds = accVectInt(std::vector<int>(Parameters::simutime));
    accNbOtherInfectedHerds = accVectInt(std::vector<int>(Parameters::simutime));
    
    accProportionOfOutgoingMovTested = accVectDouble(std::vector<double>(Parameters::simutime));
    accProportionOfOutgoingInfectedMovTested = accVectDouble(std::vector<double>(Parameters::simutime));
    
    accProportionOfIncomingMovTested = accVectDouble(std::vector<double>(Parameters::simutime));
    accProportionOfIncomingInfectedMovTested = accVectDouble(std::vector<double>(Parameters::simutime));
    
    
    boost::array<double,9> probs = {0.01,0.025,0.05,0.10,0.50,0.90,0.95,0.975,0.99};
    for (int time = 0; time < Parameters::simutime; time++) {
        accInfectedHerdsQuantiles.emplace_back(accQuantiles(tag::extended_p_square::probabilities = probs));
        accAlreadyInfectedHerdsQuantiles.emplace_back(accQuantiles(tag::extended_p_square::probabilities = probs));
        accAffectedHerdsQuantiles.emplace_back(accQuantiles(tag::extended_p_square::probabilities = probs));
        
        accInfectedProportionMetaQuantiles.emplace_back(accQuantiles(tag::extended_p_square::probabilities = probs));
        accInfectiousProportionMetaQuantiles.emplace_back(accQuantiles(tag::extended_p_square::probabilities = probs));
        accAffectedProportionMetaQuantiles.emplace_back(accQuantiles(tag::extended_p_square::probabilities = probs));
    }
    
    
    accNbHerdWithTestOfMovements = accVectInt(std::vector<int>(Parameters::simutime));
    accNbHerdWithCullingImprovement = accVectInt(std::vector<int>(Parameters::simutime));
    accNbHerdWithHygieneImprovement = accVectInt(std::vector<int>(Parameters::simutime));
    accNbHerdWithCalfManagementImprovement = accVectInt(std::vector<int>(Parameters::simutime));
    accNbHerdWithTestAndCull = accVectInt(std::vector<int>(Parameters::simutime));

    for (auto theFarmID : Parameters::vFarmID) {
        mFarms_AccVectWithinHerdPrevalence[theFarmID] = accVectDouble(std::vector<double>(Parameters::simutime));
    }
    
}



// Member functions ====================================================

void PersistentResults::updateAcc(const int& persistence, BufferPersistentResults& iBufferPersistentResults, MetapopSummary& iMetapopSummary){
    if (persistence == 1) {
        int bInfectedHerdsVariation = iBufferPersistentResults.bInfectedHerds[Parameters::simutime-1] - Parameters::nbHerdsInfected;
        vInfectedHerdsVariation.emplace_back(bInfectedHerdsVariation);
        int bAlreadyInfectedHerdsVariation = Parameters::nbHerds - iBufferPersistentResults.bNeverInfectedHerds[Parameters::simutime-1] - Parameters::nbHerdsInfected;
        vAlreadyInfectedHerdsVariation.emplace_back(bAlreadyInfectedHerdsVariation);
        
        vOutDegreeOfSelectedNode.insert(vOutDegreeOfSelectedNode.end(), iBufferPersistentResults.vBufferOutDegreeOfSelectedNode.begin(), iBufferPersistentResults.vBufferOutDegreeOfSelectedNode.end());
        vNbOutMovsOfSelectedNode.insert(vNbOutMovsOfSelectedNode.end(), iBufferPersistentResults.vBufferNbOutMovsOfSelectedNode.begin(), iBufferPersistentResults.vBufferNbOutMovsOfSelectedNode.end());
        vInDegreeOfSelectedNode.insert(vInDegreeOfSelectedNode.end(), iBufferPersistentResults.vBufferInDegreeOfSelectedNode.begin(), iBufferPersistentResults.vBufferInDegreeOfSelectedNode.end());
        vNbInMovsOfSelectedNode.insert(vNbInMovsOfSelectedNode.end(), iBufferPersistentResults.vBufferNbInMovsOfSelectedNode.begin(), iBufferPersistentResults.vBufferNbInMovsOfSelectedNode.end());
        vSizeOfSelectedNode.insert(vSizeOfSelectedNode.end(), iBufferPersistentResults.vBufferSizeOfSelectedNode.begin(), iBufferPersistentResults.vBufferSizeOfSelectedNode.end());
        vPrevalenceInSelectedNodes.insert(vPrevalenceInSelectedNodes.end(), iBufferPersistentResults.vBufferPrevalenceInSelectedNodes.begin(), iBufferPersistentResults.vBufferPrevalenceInSelectedNodes.end());
        
        vNbNewInfectedHerdsSinceSimulationStart.emplace_back(iBufferPersistentResults.bNbNewInfectedHerdsSinceSimulationStart);
        vNbNewInfectedHerdsSinceTestsStart.emplace_back(iBufferPersistentResults.bNbNewInfectedHerdsSinceTestsStart);
        
        accInfectedHerds(iBufferPersistentResults.bInfectedHerds);
        accAffectedHerds(iBufferPersistentResults.bAffectedHerds);
        accNeverInfectedHerds(iBufferPersistentResults.bNeverInfectedHerds);
        accAlreadyInfectedHerds(iBufferPersistentResults.bAlreadyInfectedHerds);
        
        accInfectedProportionMeta(U_functions::VectorDivision(iBufferPersistentResults.bInfectedProportionMeta, iBufferPersistentResults.bHeadcountMeta));
        accInfectiousProportionMeta(U_functions::VectorDivision(iBufferPersistentResults.bInfectiousProportionMeta, iBufferPersistentResults.bHeadcountMeta));
        accAffectedProportionMeta(U_functions::VectorDivision(iBufferPersistentResults.bAffectedProportionMeta, iBufferPersistentResults.bHeadcountMeta));
        
        accInfectedProportionFirst(U_functions::VectorDivision(iBufferPersistentResults.bInfectedProportionFirst, iBufferPersistentResults.bHeadcountFirst));
        accInfectiousProportionFirst(U_functions::VectorDivision(iBufferPersistentResults.bInfectiousProportionFirst, iBufferPersistentResults.bHeadcountFirst));
        accAffectedProportionFirst(U_functions::VectorDivision(iBufferPersistentResults.bAffectedProportionFirst, iBufferPersistentResults.bHeadcountFirst));
        
//        if (Parameters::withinHerdBiosecurity == false) {
//            accInfectedProportionFirstInit(U_functions::VectorDivision(iBufferPersistentResults.bInfectedProportionFirstInit, iBufferPersistentResults.bHeadcountFirstInit));
//            accInfectiousProportionFirstInit(U_functions::VectorDivision(iBufferPersistentResults.bInfectiousProportionFirstInit, iBufferPersistentResults.bHeadcountFirstInit));
//            accAffectedProportionFirstInit(U_functions::VectorDivision(iBufferPersistentResults.bAffectedProportionFirstInit, iBufferPersistentResults.bHeadcountFirstInit));
//        }
        
        
        
        accInfectedByFirst(iBufferPersistentResults.bInfectedByFirst);
        accInfectedBySecond(iBufferPersistentResults.bInfectedBySecond);
        accInfectedByExt(iBufferPersistentResults.bInfectedByExt);
                
        accInfectedByTestedMov(iBufferPersistentResults.bInfectedByTestedMov);
        
        accNbHerdsWithIc(iMetapopSummary.vNbHerdsWithIc);
        accNbHerdsWithMoreThanTwoIc(iMetapopSummary.vNbHerdsWithMoreThanTwoIc);
        
        accNbInitiallyInfectedHerds(iMetapopSummary.vNbInitiallyInfectedHerds);
        accNbOtherInfectedHerds(iMetapopSummary.vNbOtherInfectedHerds);
        
        accProportionOfOutgoingMovTested(U_functions::VectorDivision(iBufferPersistentResults.bNbOutgoingMovTested, iBufferPersistentResults.bNbOutgoingMov));
        accProportionOfOutgoingInfectedMovTested(U_functions::VectorDivision(iBufferPersistentResults.bNbOutgoingInfectedMovTested, iBufferPersistentResults.bNbOutgoingInfectedMov));
        
        accProportionOfIncomingMovTested(U_functions::VectorDivision(iBufferPersistentResults.bNbIncomingMovTested, iBufferPersistentResults.bNbIncomingMov));
        accProportionOfIncomingInfectedMovTested(U_functions::VectorDivision(iBufferPersistentResults.bNbIncomingInfectedMovTested, iBufferPersistentResults.bNbIncomingInfectedMov));
        
        vNumberOfOutgoingMovTested.emplace_back(std::accumulate(iBufferPersistentResults.bNbOutgoingMovTested.begin(), iBufferPersistentResults.bNbOutgoingMovTested.end(), 0));
        vNumberOfOutgoingMov.emplace_back(std::accumulate(iBufferPersistentResults.bNbOutgoingMov.begin(), iBufferPersistentResults.bNbOutgoingMov.end(), 0));
        
        vNumberOfIncomingMovTested.emplace_back(std::accumulate(iBufferPersistentResults.bNbIncomingMovTested.begin(), iBufferPersistentResults.bNbIncomingMovTested.end(), 0));
        vNumberOfIncomingMov.emplace_back(std::accumulate(iBufferPersistentResults.bNbIncomingMov.begin(), iBufferPersistentResults.bNbIncomingMov.end(), 0));
        
        vIntroducionOfInfectiousAnimalsIfNoTest.emplace_back(iBufferPersistentResults.bIntroducionOfInfectiousAnimalsIfNoTest);
        vIntroducionOfInfectiousAnimalsAvoided.emplace_back(iBufferPersistentResults.bIntroducionOfInfectiousAnimalsAvoided);
        
        double proportionOfIntroducionOfInfectiousAnimalsAvoided = static_cast<double>(iBufferPersistentResults.bIntroducionOfInfectiousAnimalsAvoided)/static_cast<double>(iBufferPersistentResults.bIntroducionOfInfectiousAnimalsIfNoTest);
        vProportionOfIntroducionOfInfectiousAnimalsAvoided.emplace_back(proportionOfIntroducionOfInfectiousAnimalsAvoided);
        
        for (int time = 0; time < Parameters::simutime; time++) {
            accInfectedHerdsQuantiles[time](iBufferPersistentResults.bInfectedHerds[time]);
            accAlreadyInfectedHerdsQuantiles[time](iBufferPersistentResults.bAlreadyInfectedHerds[time]);
            accAffectedHerdsQuantiles[time](iBufferPersistentResults.bAffectedHerds[time]);
            
            accInfectedProportionMetaQuantiles[time](iBufferPersistentResults.bInfectedProportionMeta[time]/iBufferPersistentResults.bHeadcountMeta[time]);
            accInfectiousProportionMetaQuantiles[time](iBufferPersistentResults.bInfectiousProportionMeta[time]/iBufferPersistentResults.bHeadcountMeta[time]);
            accAffectedProportionMetaQuantiles[time](iBufferPersistentResults.bAffectedProportionMeta[time]/iBufferPersistentResults.bHeadcountMeta[time]);
        }
        
        
        vPrevalenceInInfectedHerdsAtTheEnd.insert(vPrevalenceInInfectedHerdsAtTheEnd.end(), iBufferPersistentResults.vBufferPrevalenceInInfectedHerdsAtTheEnd.begin(), iBufferPersistentResults.vBufferPrevalenceInInfectedHerdsAtTheEnd.end());
        vvPrevalenceInInfectedHerdsAtTheEnd.emplace_back(iBufferPersistentResults.vBufferPrevalenceInInfectedHerdsAtTheEnd);

        
        for (int j = 0; j<iBufferPersistentResults.vBufferInfectionDurationWhenExtinction.size(); j++) {
            vInfectionDurationWhenExtinction.emplace_back(iBufferPersistentResults.vBufferInfectionDurationWhenExtinction[j]);
        }
        
        for (int j = 0; j<iBufferPersistentResults.vBufferInfectionDurationIfNoExtinction.size(); j++) {
            vInfectionDurationIfNoExtinction.emplace_back(iBufferPersistentResults.vBufferInfectionDurationIfNoExtinction[j]);
        }
        
        
        for (int j = 0; j<iBufferPersistentResults.vBufferExtinctionBefore1Years.size(); j++) {
            vExtinctionBefore1Years.emplace_back(iBufferPersistentResults.vBufferExtinctionBefore1Years[j]);
        }
        
        
        for (int j = 0; j<iBufferPersistentResults.vBufferExtinctionBefore2Years.size(); j++) {
            vExtinctionBefore2Years.emplace_back(iBufferPersistentResults.vBufferExtinctionBefore2Years[j]);
        }
        
        
        for (int j = 0; j<iBufferPersistentResults.vBufferExtinctionBefore3Years.size(); j++) {
            vExtinctionBefore3Years.emplace_back(iBufferPersistentResults.vBufferExtinctionBefore3Years[j]);
        }
        
        
        for (int j = 0; j<iBufferPersistentResults.vBufferExtinctionBefore5Years.size(); j++) {
            vExtinctionBefore5Years.emplace_back(iBufferPersistentResults.vBufferExtinctionBefore5Years[j]);
        }
        
        for (int j = 0; j<iBufferPersistentResults.vBufferNoExtinctionBefore5Years.size(); j++) {
            vNoExtinctionBefore5Years.emplace_back(iBufferPersistentResults.vBufferNoExtinctionBefore5Years[j]);
        }
        
        
        
        vvInfosHerdPrevalenceAfter5Years.insert(vvInfosHerdPrevalenceAfter5Years.end(), iBufferPersistentResults.vvBufferInfosHerdPrevalenceAfter5Years.begin(), iBufferPersistentResults.vvBufferInfosHerdPrevalenceAfter5Years.end());
        
//        vvDynamicHerdPrevalenceInfectedAfter5Years.insert(vvDynamicHerdPrevalenceInfectedAfter5Years.end(), iBufferPersistentResults.vvBufferDynamicHerdPrevalenceInfectedAfter5Years.begin(), iBufferPersistentResults.vvBufferDynamicHerdPrevalenceInfectedAfter5Years.end());
//        vvDynamicHerdPrevalenceInfectiousAfter5Years.insert(vvDynamicHerdPrevalenceInfectiousAfter5Years.end(), iBufferPersistentResults.vvBufferDynamicHerdPrevalenceInfectiousAfter5Years.begin(), iBufferPersistentResults.vvBufferDynamicHerdPrevalenceInfectiousAfter5Years.end());
//        vvDynamicHerdPrevalenceAffectedAfter5Years.insert(vvDynamicHerdPrevalenceAffectedAfter5Years.end(), iBufferPersistentResults.vvBufferDynamicHerdPrevalenceAffectedAfter5Years.begin(), iBufferPersistentResults.vvBufferDynamicHerdPrevalenceAffectedAfter5Years.end());
        
        
//        vvPersistenceDynamicOver5Years.insert(vvPersistenceDynamicOver5Years.end(), iBufferPersistentResults.vvBufferPersistenceDynamicOver5Years.begin(), iBufferPersistentResults.vvBufferPersistenceDynamicOver5Years.end());
        
        vvPersistenceOver5Years.insert(vvPersistenceOver5Years.end(), iBufferPersistentResults.vvBufferPersistenceOver5Years.begin(), iBufferPersistentResults.vvBufferPersistenceOver5Years.end());
        
        if (Parameters::withinHerdBiosecurity == false) {
            for (int j = 0; j<iBufferPersistentResults.vBufferSourceAndDestinationOfInfectedMovements_SourceIsFirst.size(); j++) {
                vSourceAndDestinationOfInfectedMovements_SourceIsFirst.emplace_back(iBufferPersistentResults.vBufferSourceAndDestinationOfInfectedMovements_SourceIsFirst[j]);
            }
            
            for (int j = 0; j<iBufferPersistentResults.vBufferSourceAndDestinationOfInfectedMovements_SourceIsSecond.size(); j++) {
                vSourceAndDestinationOfInfectedMovements_SourceIsSecond.emplace_back(iBufferPersistentResults.vBufferSourceAndDestinationOfInfectedMovements_SourceIsSecond[j]);
            }
            
            for (int j = 0; j<iBufferPersistentResults.vBufferSourceAndDestinationOfInfectedMovements_SourceIsExt.size(); j++) {
                vSourceAndDestinationOfInfectedMovements_SourceIsExt.emplace_back(iBufferPersistentResults.vBufferSourceAndDestinationOfInfectedMovements_SourceIsExt[j]);
            }
        }
        
        for (int j = 0; j<iBufferPersistentResults.vBufferNbInfectedMovementsPerHerd.size(); j++) {
            vNbInfectedMovementsPerHerd.emplace_back(iBufferPersistentResults.vBufferNbInfectedMovementsPerHerd[j]);
        }
        
        
        vNumberOfTSaleByPrimary.emplace_back(iBufferPersistentResults.bNumberOfTSaleByPrimary);
        vNumberOfLSaleByPrimary.emplace_back(iBufferPersistentResults.bNumberOfLSaleByPrimary);
        vNumberOfIsSaleByPrimary.emplace_back(iBufferPersistentResults.bNumberOfIsSaleByPrimary);
        
        vNumberOfNewInfectionFromPrimary.emplace_back(iBufferPersistentResults.bNumberOfNewInfectionFromPrimary);
        
        vvMatrixOfInfectedHerdsAfterHalfTheSimulationTime.emplace_back(iBufferPersistentResults.vBufferMatrixOfInfectedHerdsAfterHalfTheSimulationTime);
        vvMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime.emplace_back(iBufferPersistentResults.vBufferMatrixOfInfectedHerdsAtTheEndOfTheSimulationTime);
        vvMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime.emplace_back(iBufferPersistentResults.vBufferMatrixOfAlreadyInfectedHerdsAfterHalfTheSimulationTime);
        vvMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime.emplace_back(iBufferPersistentResults.vBufferMatrixOfAlreadyInfectedHerdsAtTheEndOfTheSimulationTime);
        
        
        vvMatrixOfNumberOfInfection.emplace_back(iBufferPersistentResults.vBufferMatrixOfNumberOfInfection);
        vvMatrixOfNumberOfInfectedAnimalsPurchased.emplace_back(iBufferPersistentResults.vBufferMatrixOfNumberOfInfectedAnimalsPurchased);
        vvMatrixOfNumberOfWeeksWithInfection.emplace_back(iBufferPersistentResults.vBufferMatrixOfNumberOfWeeksWithInfection);
        
        vvPrimaryCaseMatrix.emplace_back(iBufferPersistentResults.vBufferPrimaryCaseMatrix);
                
//        vvMatrixOfNumberOfAnimalsTestedMovIn.emplace_back(iBufferPersistentResults.vBufferMatrixOfNumberOfAnimalsTestedMovIn);
//        vvMatrixOfNumberOfAnimalsTestedMovOut.emplace_back(iBufferPersistentResults.vBufferMatrixOfNumberOfAnimalsTestedMovOut);
        vvMatrixOfNumberOfIcAnimalsCulled.emplace_back(iBufferPersistentResults.vBufferMatrixOfNumberOfIcAnimalsCulled);
        vvMatrixOfNumberOfAnimalsCulledDuringTestAndCull.emplace_back(iBufferPersistentResults.vBufferMatrixOfNumberOfAnimalsCulledDuringTestAndCull);
        vvMatrixOfNumberOfAnimalsTestedDuringTestAndCull.emplace_back(iBufferPersistentResults.vBufferMatrixOfNumberOfAnimalsTestedDuringTestAndCull);
        
        
        vMedianInfectedHerds.emplace_back(extended_p_square(accInfectedHerdsQuantiles[Parameters::simutime-1])[4]);
        vInfectedHerds.emplace_back(iBufferPersistentResults.bInfectedHerds[iBufferPersistentResults.bInfectedHerds.size()-1]);
        vAlreadyInfectedHerds.emplace_back(iBufferPersistentResults.bAlreadyInfectedHerds[iBufferPersistentResults.bAlreadyInfectedHerds.size()-1]);
        vAffectedHerds.emplace_back(iBufferPersistentResults.bAffectedHerds[iBufferPersistentResults.bAffectedHerds.size()-1]);


        double infectedPropMeta = iBufferPersistentResults.bInfectedProportionMeta[iBufferPersistentResults.bInfectedProportionMeta.size()-1] / iBufferPersistentResults.bHeadcountMeta[iBufferPersistentResults.bHeadcountMeta.size()-1];
        double infectiousPropMeta = iBufferPersistentResults.bInfectiousProportionMeta[iBufferPersistentResults.bInfectiousProportionMeta.size()-1] / iBufferPersistentResults.bHeadcountMeta[iBufferPersistentResults.bHeadcountMeta.size()-1];
        double affectedPropMeta = iBufferPersistentResults.bAffectedProportionMeta[iBufferPersistentResults.bAffectedProportionMeta.size()-1] / iBufferPersistentResults.bHeadcountMeta[iBufferPersistentResults.bHeadcountMeta.size()-1];

        vInfectedProportionMeta.emplace_back(infectedPropMeta);
        vInfectiousProportionMeta.emplace_back(infectiousPropMeta);
        vAffectedProportionMeta.emplace_back(affectedPropMeta);
        
        
        
        accNbHerdWithTestOfMovements(iMetapopSummary.vNbHerdsWithTm);
        accNbHerdWithCullingImprovement(iMetapopSummary.vNbHerdsWithCu);
        accNbHerdWithHygieneImprovement(iMetapopSummary.vNbHerdsWithH);
        accNbHerdWithCalfManagementImprovement(iMetapopSummary.vNbHerdsWithCm);
        accNbHerdWithTestAndCull(iMetapopSummary.vNbHerdsWithTc);



        vvMatrixOfWithinHerdPrevalenceOverTime.emplace_back(U_functions::VectorDivision(iMetapopSummary.vPrevalenceIntraHerd, iMetapopSummary.vNbHerdInfected));

        for (auto theFarmID : Parameters::vFarmID) {
            mFarms_AccVectWithinHerdPrevalence[theFarmID](iBufferPersistentResults.buffer_mFarms_vectPrevalence[theFarmID]);
        }
        
    }
}




