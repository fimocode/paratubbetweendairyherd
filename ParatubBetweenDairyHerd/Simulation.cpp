//
//  Simulation.cpp
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 07/01/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#include "Simulation.h"

// Constructor =========================================================
Simulation::Simulation(){
    
}

// Member functions ====================================================
void Simulation::Simulate(){
    std::cout << std::endl << "Start!" << std::endl;
    
    std::cout << std::endl << "- Run" << std::endl;
    iBetweenHerdDynamic.Run();

    std::cout << std::endl << "- SaveResults" << std::endl;
//    iBetweenHerdDynamic.SaveResults();

    iBetweenHerdDynamic.ExecutionTimeDisplay();
    
    std::cout << std::endl << "End!" << std::endl;
}
