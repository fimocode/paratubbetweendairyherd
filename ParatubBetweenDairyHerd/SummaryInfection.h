//
//  SummaryInfection.h
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 02/08/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_SummaryInfection_h
#define ParatubBetweenDairyHerd_SummaryInfection_h

#include "Parameters.h"

struct SummaryInfection {
    // Constructor =========================================================
    SummaryInfection();
    
    // Variables ===========================================================
    std::vector<int> ageAtInfection;
    std::vector<int> ageAtInfection_inutero;
    std::vector<int> ageAtInfection_colostrum;
    std::vector<int> ageAtInfection_milk;
    std::vector<int> ageAtInfection_local;
    std::vector<int> ageAtInfection_global;
    
    std::vector<int> IncidenceT;
    std::vector<int> transmissionRoutes_inutero;
    std::vector<int> transmissionRoutes_colostrum;
    std::vector<int> transmissionRoutes_milk;
    std::vector<int> transmissionRoutes_local;
    std::vector<int> transmissionRoutes_global;
    
    std::vector<int> IncidenceIs;
    std::vector<int> IncidenceIc;
    
    std::vector<int> persistence;
    
    // Member functions ====================================================
    void persistenceUpdate(const int& t);
    
    void persistencePastUpdate(const int& t);
    
};

#endif
