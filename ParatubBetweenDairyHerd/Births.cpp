//
//  Births.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 25/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "Births.h"

// Constructor =========================================================
Births::Births(){
//    Lact.resize(6,0);
//    bsum = 0;
    bS = 0;
//    bT = 0;
//    t = 0;

//    birthsOverTimeS.resize(Parameters::simutime,0);
//    birthsOverTimeT.resize(Parameters::simutime,0);
}


// Member functions ====================================================
//void Births::update(gsl_rng * randomGenerator, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd, const int& tnow, const AgeCompartments& iCompartment, const double& paramlacta, const SummaryDynamic& iSummaryDynamic){
//    t = tnow;
//    Lact[0] = iCompartment.past[Parameters::ageFirstCalving-1];
//    
//    Lact[1] = gsl_ran_binomial(randomGenerator, 1/paramlacta, iCompartment.past[Parameters::ageFirstCalving]);
//    Lact[2] = gsl_ran_binomial(randomGenerator, 1/paramlacta, iCompartment.past[Parameters::ageFirstCalving+1]);
//    Lact[3] = gsl_ran_binomial(randomGenerator, 1/paramlacta, iCompartment.past[Parameters::ageFirstCalving+2]);
//    Lact[4] = gsl_ran_binomial(randomGenerator, 1/paramlacta, iCompartment.past[Parameters::ageFirstCalving+3]);
//    Lact[5] = gsl_ran_binomial(randomGenerator, 1/paramlacta, iCompartment.past[Parameters::ageFirstCalving+4]);
//    
////    bsum = std::accumulate(Lact.begin(), Lact.end(), 0);
//    bsum = iParametersSpecificToEachHerd.vAnnualBirthEvents[tnow-1];
//    
//    
//    if (bsum>0) {
//        int bufferBirths = gsl_ran_binomial(randomGenerator, iParametersSpecificToEachHerd.sexRatio, bsum); bsum = bufferBirths;
////        int bufferBirths = bsum;
//        bS = gsl_ran_binomial(randomGenerator, 1-iParametersSpecificToEachHerd.deathRateBirth, bufferBirths);
//    } else {
//        bS=0;
//    }
//}


//void Births::inutero(gsl_rng * randomGenerator, const double& paraminfinutero){
//    if (bS>0) {
//        bT = gsl_ran_binomial(randomGenerator, paraminfinutero, bS);
//        bS -=bT;
//    } else {
//        bT=0;
//    }
//}
//
//
//void Births::transfer(AgeCompartments& iCompartmentSR, AgeCompartments& iCompartmentT, SummaryInfection& DynInf){
//    iCompartmentSR.present[0]+=bS;
//    iCompartmentT.present[0]+=bT;
//    
////    birthsOverTimeS[t] += bS;
////    birthsOverTimeT[t] += bT;
//    
//    DynInf.ageAtInfection_inutero[0] += bT;
//    DynInf.ageAtInfection[0] += bT;
//    DynInf.transmissionRoutes_inutero[t] += bT;
//    DynInf.IncidenceT[t] += bT;
//}


//void Births::setLact(const int& position, const int& quantity){
//    assert(position>=0 & position < Lact.size());
//    Lact[position] = quantity;
//}


void Births::setBirthsS(const int& quantity){
    bS = quantity;
}



