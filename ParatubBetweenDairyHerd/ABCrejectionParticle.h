//
//  ABCrejectionParticle.h
//  ParatubBetweenDairyHerd
//
//  Created by Gael Beaunée on 27/05/2015.
//  Copyright (c) 2015 Gaël Beaunée. All rights reserved.
//

#ifndef __ParatubBetweenDairyHerd__ABCrejectionParticle__
#define __ParatubBetweenDairyHerd__ABCrejectionParticle__

#include <stdio.h>

class ABCrejectionParticle {
    
public:
    // Constructor =========================================================
    ABCrejectionParticle(const double& theParam, const double& theDistance);
    
    // Variables ===========================================================
    double paramValue;
    double distance;
    
    // Member functions ====================================================
    
};

#endif /* defined(__ParatubBetweenDairyHerd__ABCrejectionParticle__) */
