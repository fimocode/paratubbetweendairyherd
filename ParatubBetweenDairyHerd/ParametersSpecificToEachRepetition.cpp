//
//  ParametersSpecificToEachRepetition.cpp
//  ParatubIntraHerd
//
//  Created by Gaël Beaunée on 25/06/13.
//  Copyright (c) 2013 Gaël Beaunée. All rights reserved.
//

#include "ParametersSpecificToEachRepetition.h"

// Constructor =========================================================
ParametersSpecificToEachRepetition::ParametersSpecificToEachRepetition(int rep){
    //Seeding the random number generator
    gsl_rng_env_setup();
    const gsl_rng_type * T;
    //T=gsl_rng_default;
    T=gsl_rng_mt19937;
    randomGenerator=gsl_rng_alloc(T);
    Seed = Parameters::Seed * rep + rep;
    if (Parameters::ABCsmc == true) {
        // Seed += Parameters::ABCsmcSequenceNumber * std::time(NULL);
        Seed += Parameters::ABCsmcSequenceNumber;
    }
    
    if (Parameters::ABCcompleteTrajectories == true) {
        // Seed += Parameters::ABCcompleteTrajectoriesSequenceNumber * std::time(NULL);
        Seed += Parameters::ABCcompleteTrajectoriesSequenceNumber;
    }
    gsl_rng_set(randomGenerator, Seed);
    //    std::cout << "Seed : " << Seed << std::endl;
    
}


// Member functions ====================================================
void ParametersSpecificToEachRepetition::setParameterValuesForABC(const int& runNumber, std::map<std::string, ABCsmcSetOfPreviousParticles>& previousParticles){
    
    if (Parameters::ABCrejection == true) {
        probaToPurchaseInfectedAnimal_initialValue = gsl_ran_flat(randomGenerator, Parameters::probaToPurchaseInfectedAnimal_initialValue_min, Parameters::probaToPurchaseInfectedAnimal_initialValue_max);
        probaToPurchaseInfectedAnimal_slope = gsl_ran_flat(randomGenerator, Parameters::probaToPurchaseInfectedAnimal_slope_min, Parameters::probaToPurchaseInfectedAnimal_slope_max);
        abcTestSensitivity = gsl_ran_flat(randomGenerator, Parameters::abcTestSensitivity_min, Parameters::abcTestSensitivity_max);
        abcInfGlobalEnv_log = gsl_ran_flat(randomGenerator, std::log10(Parameters::abcInfGlobalEnv_min), std::log10(Parameters::abcInfGlobalEnv_max));
        abcInfGlobalEnv = std::pow(10, abcInfGlobalEnv_log);
        abcInfGlobalEnvIfCalfManagementImprovement = abcInfGlobalEnv - (abcInfGlobalEnv*Parameters::decreaseOfAdultToCalfContact);
    }
    
    if (Parameters::ABCsmc == true) {
        if (runNumber == 0) {
            std::cout << "setParameterValuesForABC error!\n" << std::endl;
            std::exit(EXIT_FAILURE);
        } else if (runNumber == 1) {
            getValueForProbaToPurchaseInfectedAnimal();
            getValueAbcTestSensitivity();
            getValueAbcInfGlobalEnv();
        } else { // iABCInfosAndResults.runNumber > 1
            getPerturbedValueForProbaToPurchaseInfectedAnimal(previousParticles);
            getPerturbedValueAbcTestSensitivity(previousParticles);
            getPerturbedValueAbcInfGlobalEnv(previousParticles);
        }
    }
    
    if (Parameters::ABCcompleteTrajectories == true | Parameters::computeSummaryStatistics == true) {
        probaToPurchaseInfectedAnimal_initialValue = Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation_initialValue;
        probaToPurchaseInfectedAnimal_slope = Parameters::probaToPurchaseInfectedAnimalOutsideTheMetapopulation_slope;
        abcTestSensitivity = Parameters::abcTestSensitivity;
        abcInfGlobalEnv_log = std::log10(Parameters::infGlobalEnv);
        abcInfGlobalEnv = Parameters::infGlobalEnv;
        abcInfGlobalEnvIfCalfManagementImprovement = abcInfGlobalEnv - (abcInfGlobalEnv*Parameters::decreaseOfAdultToCalfContact);
    }
    
}


void ParametersSpecificToEachRepetition::getValueForProbaToPurchaseInfectedAnimal(){
    probaToPurchaseInfectedAnimal_initialValue = gsl_ran_flat(randomGenerator, Parameters::probaToPurchaseInfectedAnimal_initialValue_min, Parameters::probaToPurchaseInfectedAnimal_initialValue_max);
//    std::cout << probaToPurchaseInfectedAnimal << std::endl;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PreviousValue = probaToPurchaseInfectedAnimal_initialValue;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PreviousWeight = 1.0;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PreviousNormalizedWeight = 1.0;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PerturbedValue = probaToPurchaseInfectedAnimal_initialValue;
    
    
    probaToPurchaseInfectedAnimal_slope = gsl_ran_flat(randomGenerator, Parameters::probaToPurchaseInfectedAnimal_slope_min, Parameters::probaToPurchaseInfectedAnimal_slope_max);
    //    std::cout << probaToPurchaseInfectedAnimal_slope << std::endl;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PreviousValue = probaToPurchaseInfectedAnimal_slope;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PreviousWeight = 1.0;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PreviousNormalizedWeight = 1.0;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PerturbedValue = probaToPurchaseInfectedAnimal_slope;
}


void ParametersSpecificToEachRepetition::getPerturbedValueForProbaToPurchaseInfectedAnimal(std::map<std::string, ABCsmcSetOfPreviousParticles>& previousParticles){
//    std::random_device rd;
//    std::mt19937 gen(rd());
    std::mt19937 gen_initVal(static_cast<int>(Seed));
    std::discrete_distribution<> d_initVal(previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleNormalizedWeights.begin(), previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleNormalizedWeights.end());
    
    double previousValue_initVal;
    double perturbedValue_initVal;
    int index_initVal;
    
    index_initVal = d_initVal(gen_initVal);
//    index = static_cast<int>(gsl_ran_discrete (randomGenerator, gsl_ran_discrete_preproc (previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleNormalizedWeights.size(), &previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleNormalizedWeights[0])));
    previousValue_initVal = previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleValues[index_initVal];
    perturbedValue_initVal = previousValue_initVal + gsl_ran_gaussian(randomGenerator, previousParticles["probaToPurchaseInfectedAnimal_initialValue"].empiricalStandardDeviation * Parameters::perturbationKernelparameter);
    
    if (Parameters::probaToPurchaseInfectedAnimal_initialValue_min < Parameters::probaToPurchaseInfectedAnimal_initialValue_max) {
        while (perturbedValue_initVal < Parameters::probaToPurchaseInfectedAnimal_initialValue_min | perturbedValue_initVal > Parameters::probaToPurchaseInfectedAnimal_initialValue_max) {
            index_initVal = d_initVal(gen_initVal);
            //        index = static_cast<int>(gsl_ran_discrete (randomGenerator, gsl_ran_discrete_preproc (previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleNormalizedWeights.size(), &previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleNormalizedWeights[0])));
            previousValue_initVal = previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleValues[index_initVal];
            perturbedValue_initVal = previousValue_initVal + gsl_ran_gaussian(randomGenerator, previousParticles["probaToPurchaseInfectedAnimal_initialValue"].empiricalStandardDeviation * Parameters::perturbationKernelparameter);
        }
    } else if (Parameters::probaToPurchaseInfectedAnimal_initialValue_min == Parameters::probaToPurchaseInfectedAnimal_initialValue_max) {
        perturbedValue_initVal = Parameters::probaToPurchaseInfectedAnimal_initialValue_min; // = previousValue;
    } else {
        std::cout << "getPerturbedValueForProbaToPurchaseInfectedAnimal (initialValue) (smc) error!\n" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    
    probaToPurchaseInfectedAnimal_initialValue = perturbedValue_initVal;
    
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PreviousValue = previousValue_initVal;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PreviousWeight = previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleWeights[index_initVal];
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PreviousNormalizedWeight = previousParticles["probaToPurchaseInfectedAnimal_initialValue"].previousParticleNormalizedWeights[index_initVal];
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PerturbedValue = perturbedValue_initVal;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_initialValue"].PerturbedWeight = 0.0;
    
    ////
    
    std::mt19937 gen_slope(static_cast<int>(Seed));
    std::discrete_distribution<> d_slope(previousParticles["probaToPurchaseInfectedAnimal_slope"].previousParticleNormalizedWeights.begin(), previousParticles["probaToPurchaseInfectedAnimal_slope"].previousParticleNormalizedWeights.end());
    
    double previousValue_slope;
    double perturbedValue_slope;
    int index_slope;
    
    index_slope = d_slope(gen_slope);
    //    index = static_cast<int>(gsl_ran_discrete (randomGenerator, gsl_ran_discrete_preproc (previousParticles["probaToPurchaseInfectedAnimal"].previousParticleNormalizedWeights.size(), &previousParticles["probaToPurchaseInfectedAnimal"].previousParticleNormalizedWeights[0])));
    previousValue_slope = previousParticles["probaToPurchaseInfectedAnimal_slope"].previousParticleValues[index_slope];
    perturbedValue_slope = previousValue_slope + gsl_ran_gaussian(randomGenerator, previousParticles["probaToPurchaseInfectedAnimal_slope"].empiricalStandardDeviation * Parameters::perturbationKernelparameter);
    
    if (Parameters::probaToPurchaseInfectedAnimal_slope_min < Parameters::probaToPurchaseInfectedAnimal_slope_max) {
        while (perturbedValue_slope < Parameters::probaToPurchaseInfectedAnimal_slope_min | perturbedValue_slope > Parameters::probaToPurchaseInfectedAnimal_slope_max) {
            index_slope = d_slope(gen_slope);
            //        index = static_cast<int>(gsl_ran_discrete (randomGenerator, gsl_ran_discrete_preproc (previousParticles["probaToPurchaseInfectedAnimal"].previousParticleNormalizedWeights.size(), &previousParticles["probaToPurchaseInfectedAnimal"].previousParticleNormalizedWeights[0])));
            previousValue_slope = previousParticles["probaToPurchaseInfectedAnimal_slope"].previousParticleValues[index_slope];
            perturbedValue_slope = previousValue_slope + gsl_ran_gaussian(randomGenerator, previousParticles["probaToPurchaseInfectedAnimal_slope"].empiricalStandardDeviation * Parameters::perturbationKernelparameter);
        }
    } else if (Parameters::probaToPurchaseInfectedAnimal_slope_min == Parameters::probaToPurchaseInfectedAnimal_slope_max) {
        perturbedValue_slope = Parameters::probaToPurchaseInfectedAnimal_slope_min; // = previousValue;
    } else {
        std::cout << "getPerturbedValueForProbaToPurchaseInfectedAnimal (slope) (smc) error!\n" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    
    probaToPurchaseInfectedAnimal_slope = perturbedValue_slope;
    
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PreviousValue = previousValue_slope;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PreviousWeight = previousParticles["probaToPurchaseInfectedAnimal_slope"].previousParticleWeights[index_slope];
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PreviousNormalizedWeight = previousParticles["probaToPurchaseInfectedAnimal_slope"].previousParticleNormalizedWeights[index_slope];
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PerturbedValue = perturbedValue_slope;
    theABCsmcParticle.parameters["probaToPurchaseInfectedAnimal_slope"].PerturbedWeight = 0.0;
}


void ParametersSpecificToEachRepetition::getValueAbcTestSensitivity(){
    abcTestSensitivity = gsl_ran_flat(randomGenerator, Parameters::abcTestSensitivity_min, Parameters::abcTestSensitivity_max);
    //    std::cout << abcTestSensitivity << std::endl;
    
    theABCsmcParticle.parameters["abcTestSensitivity"].PreviousValue = abcTestSensitivity;
    theABCsmcParticle.parameters["abcTestSensitivity"].PreviousWeight = 1.0;
    theABCsmcParticle.parameters["abcTestSensitivity"].PreviousNormalizedWeight = 1.0;
    theABCsmcParticle.parameters["abcTestSensitivity"].PerturbedValue = abcTestSensitivity;
}


void ParametersSpecificToEachRepetition::getPerturbedValueAbcTestSensitivity(std::map<std::string, ABCsmcSetOfPreviousParticles>& previousParticles){
//    std::random_device rd;
//    std::mt19937 gen(rd());
    std::mt19937 gen(static_cast<int>(Seed));
    std::discrete_distribution<> d(previousParticles["abcTestSensitivity"].previousParticleNormalizedWeights.begin(), previousParticles["abcTestSensitivity"].previousParticleNormalizedWeights.end());
    
    double previousValue;
    double perturbedValue;
    int index;
    
    index = d(gen);
    previousValue = previousParticles["abcTestSensitivity"].previousParticleValues[index];
    perturbedValue = previousValue + gsl_ran_gaussian(randomGenerator, previousParticles["abcTestSensitivity"].empiricalStandardDeviation * Parameters::perturbationKernelparameter);
    
    if (Parameters::abcTestSensitivity_min < Parameters::abcTestSensitivity_max) {
        while (perturbedValue < Parameters::abcTestSensitivity_min | perturbedValue > Parameters::abcTestSensitivity_max) {
            index = d(gen);
            previousValue = previousParticles["abcTestSensitivity"].previousParticleValues[index];
            perturbedValue = previousValue + gsl_ran_gaussian(randomGenerator, previousParticles["abcTestSensitivity"].empiricalStandardDeviation * Parameters::perturbationKernelparameter);
        }
    } else if (Parameters::abcTestSensitivity_min == Parameters::abcTestSensitivity_max) {
        perturbedValue = Parameters::abcTestSensitivity_min; // = previousValue;
    } else {
        std::cout << "getPerturbedValueAbcTestSensitivity (smc) error!\n" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    
    abcTestSensitivity = perturbedValue;
    
    theABCsmcParticle.parameters["abcTestSensitivity"].PreviousValue = previousValue;
    theABCsmcParticle.parameters["abcTestSensitivity"].PreviousWeight = previousParticles["abcTestSensitivity"].previousParticleWeights[index];
    theABCsmcParticle.parameters["abcTestSensitivity"].PreviousNormalizedWeight = previousParticles["abcTestSensitivity"].previousParticleNormalizedWeights[index];
    theABCsmcParticle.parameters["abcTestSensitivity"].PerturbedValue = perturbedValue;
    theABCsmcParticle.parameters["abcTestSensitivity"].PerturbedWeight = 0.0;
}


void ParametersSpecificToEachRepetition::getValueAbcInfGlobalEnv(){
    abcInfGlobalEnv_log = gsl_ran_flat(randomGenerator, std::log10(Parameters::abcInfGlobalEnv_min), std::log10(Parameters::abcInfGlobalEnv_max));
    abcInfGlobalEnv = std::pow(10, abcInfGlobalEnv_log);
    abcInfGlobalEnvIfCalfManagementImprovement = abcInfGlobalEnv - (abcInfGlobalEnv*Parameters::decreaseOfAdultToCalfContact);
    //    std::cout << abcInfGlobalEnv << std::endl;
    
    theABCsmcParticle.parameters["abcInfGlobalEnv"].PreviousValue = abcInfGlobalEnv_log;
    theABCsmcParticle.parameters["abcInfGlobalEnv"].PreviousWeight = 1.0;
    theABCsmcParticle.parameters["abcInfGlobalEnv"].PreviousNormalizedWeight = 1.0;
    theABCsmcParticle.parameters["abcInfGlobalEnv"].PerturbedValue = abcInfGlobalEnv_log;
}


void ParametersSpecificToEachRepetition::getPerturbedValueAbcInfGlobalEnv(std::map<std::string, ABCsmcSetOfPreviousParticles>& previousParticles){
//    std::random_device rd;
//    std::mt19937 gen(rd());
    std::mt19937 gen(static_cast<int>(Seed));
    std::discrete_distribution<> d(previousParticles["abcInfGlobalEnv"].previousParticleNormalizedWeights.begin(), previousParticles["abcInfGlobalEnv"].previousParticleNormalizedWeights.end());
    
    double previousValue;
    double perturbedValue;
    int index;
    
    index = d(gen);
    previousValue = previousParticles["abcInfGlobalEnv"].previousParticleValues[index];
    perturbedValue = previousValue + gsl_ran_gaussian(randomGenerator, previousParticles["abcInfGlobalEnv"].empiricalStandardDeviation * Parameters::perturbationKernelparameter);
    
    if (Parameters::abcInfGlobalEnv_min < Parameters::abcInfGlobalEnv_max) {
        while ((perturbedValue < std::log10(Parameters::abcInfGlobalEnv_min)) | (perturbedValue > std::log10(Parameters::abcInfGlobalEnv_max))) {
            index = d(gen);
            previousValue = previousParticles["abcInfGlobalEnv"].previousParticleValues[index];
            perturbedValue = previousValue + gsl_ran_gaussian(randomGenerator, previousParticles["abcInfGlobalEnv"].empiricalStandardDeviation * Parameters::perturbationKernelparameter);
        }
    } else if (Parameters::abcInfGlobalEnv_min == Parameters::abcInfGlobalEnv_max) {
        perturbedValue = std::log10(Parameters::abcInfGlobalEnv_min); // = previousValue;
    } else {
        std::cout << "getPerturbedValueAbcInfGlobalEnv (smc) error!\n" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    
    abcInfGlobalEnv_log = perturbedValue;
    abcInfGlobalEnv = std::pow(10, abcInfGlobalEnv_log);
    abcInfGlobalEnvIfCalfManagementImprovement = abcInfGlobalEnv - (abcInfGlobalEnv*Parameters::decreaseOfAdultToCalfContact);
    
    theABCsmcParticle.parameters["abcInfGlobalEnv"].PreviousValue = previousValue;
    theABCsmcParticle.parameters["abcInfGlobalEnv"].PreviousWeight = previousParticles["abcInfGlobalEnv"].previousParticleWeights[index];
    theABCsmcParticle.parameters["abcInfGlobalEnv"].PreviousNormalizedWeight = previousParticles["abcInfGlobalEnv"].previousParticleNormalizedWeights[index];
    theABCsmcParticle.parameters["abcInfGlobalEnv"].PerturbedValue = perturbedValue;
    theABCsmcParticle.parameters["abcInfGlobalEnv"].PerturbedWeight = 0.0;
}


void ParametersSpecificToEachRepetition::updateValueForProbaToPurchaseInfectedAnimal(const int& timeStep){
    probaToPurchaseInfectedAnimal_currentValue = std::max(0.0, std::min(1.0, probaToPurchaseInfectedAnimal_initialValue + probaToPurchaseInfectedAnimal_slope * timeStep));
    
//    if (probaToPurchaseInfectedAnimal_currentValue > 1.0) {
//        probaToPurchaseInfectedAnimal_currentValue = 1.0;
//    }
//    
//    if (probaToPurchaseInfectedAnimal_currentValue < 0.0) {
//        probaToPurchaseInfectedAnimal_currentValue = 0.0;
//    }
}



