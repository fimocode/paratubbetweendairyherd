//
//  MetapopSummary.h
//  ParatubBetweenDairyHerd
//
//  Created by Gaël Beaunée on 13/07/2014.
//  Copyright (c) 2014 Gaël Beaunée. All rights reserved.
//

#ifndef ParatubBetweenDairyHerd_MetapopSummary_h
#define ParatubBetweenDairyHerd_MetapopSummary_h

#include <iostream>
#include <vector>

#include "Parameters.h"
#include "ParametersSpecificToEachRepetition.h"
#include "ParametersSpecificToEachHerd.h"
//#include "DairyHerd.h"

class MetapopSummary {
    
public:
    // Variables ===========================================================
    std::vector<double> presentSR;
    std::vector<double> presentT;
    std::vector<double> presentL;
    std::vector<double> presentIs;
    std::vector<double> presentIc;
    
    std::vector<double> pastSR;
    std::vector<double> pastT;
    std::vector<double> pastL;
    std::vector<double> pastIs;
    std::vector<double> pastIc;
    
    double pastInfected;
    double pastInfectious;
    double pastAffected;
    double pastHeadcount;
    
    int presentNbHerdsWithIc;
    int presentNbHerdsWithMoreThanTwoIc;
    
    int pastNbHerdsWithIc;
    int pastNbHerdsWithMoreThanTwoIc;
    
    std::vector<int> vNbHerdsWithIc; //
    std::vector<int> vNbHerdsWithMoreThanTwoIc; //
    
    int NbInitiallyInfectedHerds;
    int NbOtherInfectedHerds;
    
    std::vector<int> vNbInitiallyInfectedHerds; //
    std::vector<int> vNbOtherInfectedHerds; //
    
    
    
    std::vector<int> vNbHerdsWithTm;
    std::vector<int> vNbHerdsWithCu;
    std::vector<int> vNbHerdsWithH;
    std::vector<int> vNbHerdsWithCm;
    std::vector<int> vNbHerdsWithTc;



    std::vector<double> vPrevalenceIntraHerd;
    std::vector<double> vNbHerdInfected;
    
    
    // Constructor =========================================================
    MetapopSummary();
    
    // Member functions ====================================================
    void Clear(ParametersSpecificToEachRepetition& ParamRep);
//    void Update(const DairyHerd& vHerd);
    void Resume(const int& timeStep);
    void initialCopy(const int& timeStep);
    void updateControlStrategyCensus(const int& timeStep, const ParametersSpecificToEachHerd& iParametersSpecificToEachHerd);
};

#endif /* defined(__ParatubBetweenDairyHerd__MetapopSummary__) */
